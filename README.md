# SLSA Tools

## Introduction

This project contains various tools and libraries to generate and process [SLSA attestations](https://slsa.dev/attestation-model):

* Several command line tools to verify and dump SLSA provenance attestations
* A Java library to parse and generate SLSA provenance attestations

## Usage

In order to use the various command line tool, generate a distribution first:

```shell
$ ./gradlew clean distZip
```

Afterwards you can extract the zip archive located at

```shell
$ ./tools/build/distributions/slsa-tools-0.1-SNAPSHOT.zip
```

and add the extract directory to your `PATH` variable.

The following tools are included atm:

* `slsa-dump.sh`: dumps the provenance attestation as contained in an `.intoto.jsonl` file
* `slsa-verify.sh`: verifies a provenance attestation with a given artifact

## Using the library

To include the `attestation` library in your project, depend on:

```xml
<dependency>
    <groupId>org.eclipsefdn.security.slsa</groupId>
    <artifactId>attestation</artifactId>
    <version>0.1-SNAPSHOT</version>
</dependency>
```

and include the following repository (snapshots only atm):

```xml
<repository>
    <id>repo.eclipse.org.slsa-tools.snapshot</id>
    <url>https://repo.eclipse.org/content/repositories/slsa-tools-snapshots/</url>
</repository>
```

## LICENSE

Licensed under Eclipse Public License v 2.0, see [LICENSE](LICENSE.md)

