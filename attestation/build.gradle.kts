plugins {
    id("library")
}

description = "Library to process SLSA provenance attestation files"

dependencies {
    api(libs.jackson.databind)
    implementation(libs.findbugs.jsr305)
    implementation(libs.jackson.databind.jsr310)
}
