/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.error;

public class Errors {
    public static final String INVALID_DSSE_ENVELOPE_FORMAT                = "invalid DSSE envelope";
    public static final String INVALID_DSSE_PAYLOAD                        = "invalid DSSE envelope payload type '%s'";
    public static final String INVALID_PROVENANCE_FORMAT                   = "invalid SLSA provenance format";

    public static final String INVALID_BUILDER_ID_FORMAT                   = "invalid builder ID format";
    public static final String INVALID_OIDC_ISSUER                         = "invalid OIDC issuer";
    public static final String MALFORMED_URI                               = "URI is malformed";
    public static final String MISMATCH_SOURCE_URI                         = "source URI does not match expected source";
    public static final String UNTRUSTED_REUSABLE_WORKFLOW                 = "untrusted reusable workflow";
    public static final String UNTRUSTED_REUSABLE_WORKFLOW_INVALID_VERSION = "untrusted reusable workflow - invalid version";

    public static final String INTERNAL_ERROR                              = "internal error";

    /** Hide constructor. */
    private Errors() {}
}
