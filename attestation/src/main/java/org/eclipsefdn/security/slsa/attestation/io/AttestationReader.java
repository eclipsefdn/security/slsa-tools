/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.io;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipsefdn.security.slsa.attestation.error.Errors;
import org.eclipsefdn.security.slsa.attestation.error.InvalidModelException;
import org.eclipsefdn.security.slsa.attestation.model.SignedAttestation;
import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.eclipsefdn.security.slsa.attestation.model.dsse.Envelope;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

public class AttestationReader {
    /**
     * Reads SLSA attestations from the given {@code inputStream}.
     *
     * @param inputStream the {@code InputStream} to read the attestations from.
     * @return a {@code List} of {@code SignedAttestation} that were read from the {@code inputStream}.
     * @throws IOException
     */
    public List<SignedAttestation> readAttestations(InputStream inputStream) throws IOException {
        try {
            List<Envelope> envelopeList = Json.readObjects(inputStream, Envelope.class);
            return envelopeList.stream().map(SignedAttestation::of).collect(Collectors.toList());
        } catch (JsonProcessingException ex) {
            throw new InvalidModelException(Errors.INVALID_DSSE_ENVELOPE_FORMAT, ex);
        }
    }
}
