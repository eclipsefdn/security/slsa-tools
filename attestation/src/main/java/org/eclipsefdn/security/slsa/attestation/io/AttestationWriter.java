/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.io;

import org.eclipsefdn.security.slsa.attestation.model.SignedAttestation;
import org.eclipsefdn.security.slsa.attestation.util.Json;

import java.io.IOException;
import java.io.OutputStream;

public class AttestationWriter {

    private final OutputStream outputStream;

    public AttestationWriter(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * Write the given SLSA attestation to the underlying {@code outputStream}.
     *
     * @param attestation the {@code SignedAttestation} to write to the stream.
     * @throws IOException
     */
    public void writeAttestation(SignedAttestation attestation) throws IOException {
        Json.writeObject(outputStream, attestation.getEnvelope());
    }
}
