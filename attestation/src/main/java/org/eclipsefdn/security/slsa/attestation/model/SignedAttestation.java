/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipsefdn.security.slsa.attestation.error.Errors;
import org.eclipsefdn.security.slsa.attestation.error.InvalidModelException;
import org.eclipsefdn.security.slsa.attestation.model.dsse.Envelope;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import org.eclipsefdn.security.slsa.attestation.util.Json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class SignedAttestation {
    private static final String INTOTO_PAYLOAD_TYPE = "application/vnd.in-toto+json";

    private final Envelope envelope;
    private final ProvenanceStatement statement;

    /**
     * Loads an SLSA provenance statement wrapped inside the given {@code envelope}.
     *
     * @return a {@link SignedAttestation}
     * @throws InvalidModelException if the wrapped provenance statement can not be deserialized.
     */
    public static SignedAttestation of(Envelope envelope) throws InvalidModelException {
        String payloadType = envelope.getPayloadType();

        if (!INTOTO_PAYLOAD_TYPE.equals(payloadType)) {
            throw new InvalidModelException(String.format(Errors.INVALID_DSSE_PAYLOAD, payloadType));
        }

        return new SignedAttestation(envelope);
    }

    public static SignedAttestation of(ProvenanceStatement statement) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Json.writeObject(baos, statement);
            byte[] encodedPayload = Base64.getEncoder().encode(baos.toByteArray());

            Envelope envelope =
                Envelope.builder()
                        .withPayloadType(INTOTO_PAYLOAD_TYPE)
                        .withPayload(new String(encodedPayload))
                        .build();

            return new SignedAttestation(envelope);
        } catch (IOException ioe) {
            throw new IllegalStateException(Errors.INTERNAL_ERROR);
        }
    }

    private SignedAttestation(Envelope envelope) throws InvalidModelException {
        this.envelope = envelope;

        try {
            byte[] decodedPayload = envelope.getDecodedPayload();
            this.statement = Json.readObject(decodedPayload, ProvenanceStatement.class);
        } catch (JsonProcessingException ex) {
            throw new InvalidModelException(Errors.INVALID_PROVENANCE_FORMAT, ex);
        } catch (IOException ioe) {
            // should not happen
            throw new InvalidModelException(Errors.INTERNAL_ERROR, ioe);
        }
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public ProvenanceStatement getStatement() {
        return statement;
    }
}
