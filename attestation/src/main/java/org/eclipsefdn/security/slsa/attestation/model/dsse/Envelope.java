/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.dsse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import org.eclipsefdn.security.slsa.attestation.util.Strings;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Model class to represent a DSSE Envelope.
 * <p>
 * The Envelope is the outermost layer of an attestation, handling authentication and serialization.
 * The format and protocol are defined in DSSE and adopted by in-toto in ITE-5.
 * <p>
 * Defined in <a href="https://github.com/secure-systems-lab/dsse/blob/master/envelope.md">...</a>
 */
public class Envelope {
    /**
     * The identifier for the type of the payload.
     */
    private String payloadType;

    /**
     * A base64-encoded string representing the payload.
     */
    private String payload;

    /**
     * A list of DSSE signatures. See: {@link Signature}
     */
    private List<Signature> signatures;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link Envelope}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private Envelope(@JsonProperty(value = "payloadType", required = true) String payloadType,
                     @JsonProperty(value = "payload", required = true)     String payload) {
        this.payloadType = payloadType;
        this.payload     = payload;
        this.signatures  = new ArrayList<>();
    }

    /**
     * Returns the identifier for the type of the payload.
     */
    public String getPayloadType() {
        return payloadType;
    }

    /**
     * Returns a base64-encoded string representing the payload.
     */
    public String getPayload() {
        return payload;
    }

    /**
     * Returns the base64-decoded payload as {@code byte[]}.
     */
    public byte[] getDecodedPayload() {
        return Base64.getDecoder().decode(getPayload());
    }

    @Nonnull
    public Collection<Signature> getSignatures() {
        return signatures;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setSignatures(@Nonnull Collection<Signature> subjects) {
        Objects.requireNonNull(subjects);
        this.signatures = new ArrayList<>(subjects);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Envelope other = (Envelope) o;
        return payloadType.equals(other.payloadType) &&
               payload.equals(other.payload)         &&
               signatures.equals(other.signatures);
    }

    @Override
    public int hashCode() {
        return Objects.hash(payloadType, payload, signatures);
    }

    @Override
    public String toString() {
        return
            String.format("Envelope[payloadType='%s', payload='%s', signatures=%d entries]",
                          payloadType,
                          Strings.shorten(payload),
                          signatures.size());
    }

    /**
     * A builder class to construct instances of {@link Envelope}.
     */
    public static class Builder {
        private String payloadType;
        private String payload;

        private Builder() {}

        /**
         * Set the {@code parameters} property to true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withPayloadType(@Nonnull String payloadType) {
            Objects.requireNonNull(payloadType);
            this.payloadType = payloadType;
            return this;
        }

        /**
         * Set the {@code parameters} property to true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withPayload(@Nonnull String payload) {
            Objects.requireNonNull(payload);
            this.payload = payload;
            return this;
        }

        /**
         * Builds a new instance of {@link Envelope} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link Envelope} instance
         */
        public Envelope build() {
            return new Envelope(payloadType, payload);
        }
    }
}
