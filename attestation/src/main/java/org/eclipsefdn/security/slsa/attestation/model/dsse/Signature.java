/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.dsse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.util.Strings;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Model class to represent a DSSE signature.
 */
public class Signature {
    /**
     * Base64 encoded signature.
     */
    private String signature;

    /**
     * *Unauthenticated* hint identifying which public key was used.
     */
    private String keyId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String certificate;

    @JsonCreator
    private Signature(@JsonProperty("sig")   String signature,
                      @JsonProperty("keyid") String keyId,
                      @JsonProperty("cert")  String certificate) {
        this.signature = signature;
        this.keyId = keyId;
        this.certificate = certificate;
    }

    public String getSignature() {
        return signature;
    }

    public String getKeyId() {
        return keyId;
    }

    public String getCertificate() {
        return certificate;
    }

    public X509Certificate getX509Certificate() throws CertificateException {
        if (certificate != null && !certificate.isEmpty()) {
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            try (InputStream is = new ByteArrayInputStream(certificate.getBytes())) {
                return (X509Certificate) factory.generateCertificate(is);
            } catch (IOException ex) {
                // ignore
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Signature other = (Signature) o;
        return signature.equals(other.signature)   &&
               Objects.equals(keyId, other.keyId) &&
               Objects.equals(certificate, other.certificate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(signature, keyId, certificate);
    }

    @Override
    public String toString() {
        String certificateString = null;

        if (certificate != null) {
            Pattern pattern = Pattern.compile("\\R");
            certificateString = pattern.splitAsStream(certificate).skip(1).findAny().orElse(null);
        }

        return
            String.format("Signature[signature='%s', keyId='%s', certificate='%s'}",
                          Strings.shorten(signature),
                          Strings.shorten(keyId),
                          Strings.shorten(certificateString));
    }
}
