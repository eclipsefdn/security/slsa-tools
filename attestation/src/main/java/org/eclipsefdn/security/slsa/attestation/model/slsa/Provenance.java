/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1.ProvenanceV0_1;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0.ProvenanceV1;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2.ProvenanceV0_2;

/**
 * The Provenance is the innermost layer of the attestation, containing arbitrary metadata about the
 * Statement's subject.
 */
@JsonSubTypes({
    @JsonSubTypes.Type(value = ProvenanceV0_1.class, name = ProvenanceV0_1.TYPE),
    @JsonSubTypes.Type(value = ProvenanceV0_2.class, name = ProvenanceV0_2.TYPE),
    @JsonSubTypes.Type(value = ProvenanceV1.class,   name = ProvenanceV1.TYPE)
})
public abstract class Provenance {

    /**
     * Returns a URI to uniquely identify the type of {@link Provenance} this instance corresponds to which is used for
     * serializing this object inside a {@link ProvenanceStatement}.
     *
     * @return the type of this {@code Provenance} as URI string
     */
    @JsonIgnore
    public abstract String getPredicateType();
}
