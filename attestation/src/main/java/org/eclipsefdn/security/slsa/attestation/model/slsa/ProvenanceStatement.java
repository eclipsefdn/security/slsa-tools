/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.Nulls;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * The ProvenanceStatement is the middle layer of the attestation, binding a particular subject
 * to a specific provenance predicate.
 */
public class ProvenanceStatement {
    public static final String TYPE = "https://in-toto.io/Statement/v0.1";

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Identifier for the schema of the Statement.
     */
    @JsonProperty(value = "_type", required = true)
    @JsonDeserialize(using = ProvenanceStatement.TypeDeserializer.class)
    private final String type;

    /**
     * Set of software artifacts that the attestation applies to. Each element represents a single
     * software artifact.
     * <p>
     * IMPORTANT: Subject artifacts are matched purely by digest, regardless of content type. If
     * this matters to you, please comment on GitHub Issue #28
     */
    @JsonProperty("subject")
    private List<Subject> subjects;

    /**
     * URI identifying the type of the Predicate.
     */
    protected String predicateType;

    /**
     * Additional parameters of the Predicate. Unset is treated the same as set-but-empty. MAY be
     * omitted if predicateType fully describes the predicate.
     */
    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "predicateType")
    @JsonProperty("predicate")
    protected Provenance provenance;

    private ProvenanceStatement() {
        this.type = TYPE;
        this.subjects = new ArrayList<>();
    }

    private ProvenanceStatement(List<Subject> subjects, Provenance provenance) {
        this.type          = TYPE;
        this.subjects      = subjects;
        this.provenance    = provenance;
        this.predicateType = provenance.getPredicateType();
    }

    public String getType() {
        return type;
    }

    @Nonnull
    public Collection<Subject> getSubjects() {
        return subjects;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setSubjects(@Nonnull List<Subject> subjects) {
        Objects.requireNonNull(subjects);
        this.subjects = subjects;
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public String getPredicateType() {
        return predicateType;
    }

    public Provenance getProvenance() {
        return provenance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProvenanceStatement other = (ProvenanceStatement) o;
        return type.equals(other.type)                   &&
               subjects.equals(other.subjects)           &&
               predicateType.equals(other.predicateType) &&
               Objects.equals(provenance, other.provenance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, subjects, predicateType, provenance);
    }

    @Override
    public String toString() {
        return String.format("ProvenanceStatement[type=%s,subjects=%s,provenance=%s]", type, subjects, provenance);
    }

    public static class Builder {
        private List<Subject> subjects;
        private Provenance    provenance;

        private Builder() {
            this.subjects = new ArrayList<>();
        }

        public Builder withSubject(@Nonnull Subject subject) {
            Objects.requireNonNull(subject);
            this.subjects.add(subject);
            return this;
        }

        public Builder withProvenance(@Nonnull Provenance provenance) {
            Objects.requireNonNull(provenance);
            this.provenance = provenance;
            return this;
        }

        public ProvenanceStatement build() {
            return new ProvenanceStatement(subjects, provenance);
        }
    }

    // Helper class for deserialization.

    static class TypeDeserializer extends StdDeserializer<String> {
        private static final long serialVersionUID = 1L;

        TypeDeserializer() {
            super((Class<String>)null);
        }

        TypeDeserializer(Class<String> clazz) {
            super(clazz);
        }

        @Override
        public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            String type = p.getValueAsString();
            if (!TYPE.equals(type)) {
                throw new JsonMappingException(p, String.format("invalid intoto statement type '%s'", type));
            }
            return type;
        }
    }
}
