/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Set of software artifacts that the attestation applies to.
 * Each element represents a single software artifact.
 */
public class Subject {
    private final String name;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final DigestSet digests;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link Subject}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private Subject(@JsonProperty("name")   String    name,
                    @JsonProperty("digest") DigestSet digests) {
        this.name    = name;
        this.digests = digests;
    }

    /**
     * Identifier to distinguish this artifact from others within the subject.
     * <p>
     * The semantics are up to the producer and consumer. Because consumers evaluate the name
     * against a policy, the name SHOULD be stable between attestations. If the name is not
     * meaningful, use "_". For example, a SLSA Provenance attestation might use the name to specify
     * output filename, expecting the consumer to only considers entries with a particular name.
     * Alternatively, a vulnerability scan attestation might use the name "_" because the results
     * apply regardless of what the artifact is named.
     * <p>
     * MUST be non-empty and unique within subject.
     */
    public String getName() {
        return name;
    }

    /**
     * Collection of cryptographic digests for the contents of this artifact.
     * <p>
     * Two DigestSets are considered matching if ANY of the fields match. The producer and consumer
     * must agree on acceptable algorithms. If there are no overlapping algorithms, the subject is
     * considered not matching.
     * <p>
     * This implementation 2 Strings instead of an enum as the key in order to facilitate future
     * extensions.
     */
    public DigestSet getDigests() {
        return digests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subject other = (Subject) o;
        return name.equals(other.name) &&
               digests.equals(other.digests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, digests);
    }

    @Override
    public String toString() {
        return String.format("Subject[name=%s,digests=%s]", name, digests);
    }

    public static class Builder {
        private String name;
        private DigestSet digests;

        private Builder() {
            digests = DigestSet.empty();
        }

        public Builder withName(@Nonnull String name) {
            Objects.requireNonNull(name);
            this.name = name;
            return this;
        }

        public Builder withDigests(@Nonnull DigestSet digests) {
            Objects.requireNonNull(digests);
            this.digests = digests;
            return this;
        }

        public Builder withDigest(@Nonnull DigestAlgorithm algorithmType, @Nonnull String hash) {
            Objects.requireNonNull(algorithmType);
            Objects.requireNonNull(hash);
            this.digests.addDigest(algorithmType.getValue(), hash);
            return this;
        }

        public Subject build() {
            return new Subject(name, digests);
        }
    }
}
