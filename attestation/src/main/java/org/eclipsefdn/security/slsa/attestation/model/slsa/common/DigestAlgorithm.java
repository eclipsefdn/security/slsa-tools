/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum with common algorithm types as contained in a {@link DigestSet}.
 */
public enum DigestAlgorithm {
    SHA256("sha256"),
    SHA224("sha224"),
    SHA384("sha384"),
    SHA512("sha512"),
    SHA512_224("sha512_224"),
    SHA512_256("sha512_256"),
    SHA3_224("sha3_224"),
    SHA3_256("sha3_256"),
    SHA3_384("sha3_384"),
    SHA3_512("sha3_512"),
    SHAKE128("shake128"),
    SHAKE256("shake256"),
    BLAKE2B("blake2B"),
    BLAKE2S("blake2S"),
    RIPEMD160("ripemd160"),
    SM3("sm3"),
    GOST("gost"),
    SHA1("sha1"),
    MD5("md5");

    private final String value;

    DigestAlgorithm(String key) {
        this.value = key;
    }

    @JsonCreator
    public static DigestAlgorithm fromString(String key) {
        return key == null ? null : DigestAlgorithm.valueOf(key.toUpperCase());
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
