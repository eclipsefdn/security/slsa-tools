/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.common;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Model class to represent a {@link Map} or digests for different algorithms.
 */
@JsonSerialize(using = DigestSet.Serializer.class)
public class DigestSet {
    @JsonValue
    private final Map<String, String> digests;

    public static DigestSet empty() {
        return new DigestSet();
    }

    public static DigestSet ofSha256Hash(String hash) {
        DigestSet digestSet = new DigestSet();
        digestSet.addDigest(DigestAlgorithm.SHA256.getValue(), hash);
        return digestSet;
    }

    private DigestSet() {
        digests = new HashMap<>();
    }

    public Iterable<DigestAlgorithm> getAvailableDigestAlgorithms() {
        return digests.keySet().stream().map(DigestAlgorithm::fromString).collect(Collectors.toList());
    }

    public Iterable<String> getAvailableAlgorithms() {
        return digests.keySet();
    }

    public boolean hasDigest(@Nonnull DigestAlgorithm algorithmType) {
        Objects.requireNonNull(algorithmType);
        return digests.containsKey(algorithmType.getValue());
    }

    public boolean hasDigest(@Nonnull String algorithm) {
        Objects.requireNonNull(algorithm);
        return digests.containsKey(algorithm);
    }

    public String getDigest(@Nonnull DigestAlgorithm algorithmType) {
        Objects.requireNonNull(algorithmType);
        return getDigest(algorithmType.getValue());
    }

    public String getDigest(@Nonnull String algorithm) {
        Objects.requireNonNull(algorithm);
        return digests.get(algorithm);
    }

    @JsonAnySetter
    public void addDigest(@Nonnull String algorithm, @Nonnull String hash) {
        Objects.requireNonNull(algorithm);
        Objects.requireNonNull(hash);
        digests.put(algorithm, hash);
    }

    public int size() {
        return digests.size();
    }

    public boolean isEmpty() {
        return digests.isEmpty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigestSet other = (DigestSet) o;
        return Objects.equals(digests, other.digests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(digests);
    }

    @Override
    public String toString() {
        return String.format("DigestSet[%d entries]", digests.size());
    }


    /**
     * A custom serializer to support {@code isEmpty} semantics during serialization.
     */
    static class Serializer extends StdSerializer<DigestSet> {
        Serializer() {
            super((Class< DigestSet>)null);
        }

        Serializer(Class<DigestSet> clazz) {
            super(clazz);
        }

        @Override
        public void serialize(DigestSet value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
            serializers.defaultSerializeValue(value.digests, gen);
        }

        @Override
        public boolean isEmpty(SerializerProvider provider, DigestSet value) {
            return value.isEmpty();
        }
    }

    /**
     * A custom deserializer that returns an empty {@code DigestSet} in case null is provided.
     */
    public static class Deserializer extends StdDeserializer<DigestSet> implements ResolvableDeserializer {
        private final JsonDeserializer<?> defaultDeserializer;

        public Deserializer(JsonDeserializer<?> defaultDeserializer) {
            super(DigestSet.class);
            this.defaultDeserializer = defaultDeserializer;
        }

        @Override
        public DigestSet deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
            return (DigestSet) defaultDeserializer.deserialize(p, ctxt);
        }

        @Override public void resolve(DeserializationContext ctxt) throws JsonMappingException {
            ((ResolvableDeserializer) defaultDeserializer).resolve(ctxt);
        }

        @Override
        public DigestSet getNullValue(DeserializationContext ctxt) {
            return DigestSet.empty();
        }

        @Override
        public Object getEmptyValue(DeserializationContext ctxt) {
            return DigestSet.empty();
        }
    }
}
