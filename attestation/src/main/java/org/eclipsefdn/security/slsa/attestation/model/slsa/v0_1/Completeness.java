/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import java.util.Objects;

/**
 * Indicates that the builder claims certain fields in this message to be complete.
 */
public class Completeness {
    /**
     * If true, the builder claims that `recipe.arguments` is complete, meaning that all external inputs
     * are properly captured in recipe.
     */
    private boolean arguments;

    /**
     * If true, the builder claims that `recipe.environment` is claimed to be complete.
     */
    private boolean environment;

    /**
     * If true, the builder claims that materials is complete, usually through some controls to
     * prevent network access. Sometimes called “hermetic”.
     */
    private boolean materials;

    public boolean isArguments() {
        return arguments;
    }

    public boolean isEnvironment() {
        return environment;
    }

    public boolean isMaterials() {
        return materials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Completeness other = (Completeness) o;
        return arguments   == other.arguments   &&
               environment == other.environment &&
               materials   == other.materials;
    }

    @Override
    public int hashCode() {
        return Objects.hash(arguments, environment, materials);
    }

    @Override
    public String toString() {
      return
          String.format("Completeness[arguments=%s, environment=%s, materials=%s]",
                        arguments, environment, materials);
    }
}
