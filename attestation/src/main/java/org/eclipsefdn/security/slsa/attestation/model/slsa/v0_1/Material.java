/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;

import java.util.Objects;

/**
 * The collection of artifacts that influenced the build including sources, dependencies, build
 * tools, base images, and so on.
 * <p>
 * This is considered to be incomplete unless metadata.completeness.materials is true. Unset or
 * null is equivalent to empty.
 */
public class Material {
    /**
     * The method by which this artifact was referenced during the build.
     *
     * @see <a href="https://github.com/in-toto/attestation/blob/main/spec/field_types.md#ResourceURI">ResourceURI</a>
     */
    private String uri;

    /**
     * Collection of cryptographic digests for the contents of this artifact.
     */
    @JsonProperty("digest")
    private DigestSet digests;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public DigestSet getDigests() {
        return digests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Material other = (Material) o;
        return Objects.equals(uri,     other.uri) &&
               Objects.equals(digests, other.digests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, digests);
    }

    @Override
    public String toString() {
        return String.format("Material[uri='%s', digests=%s]", uri, digests);
    }
}
