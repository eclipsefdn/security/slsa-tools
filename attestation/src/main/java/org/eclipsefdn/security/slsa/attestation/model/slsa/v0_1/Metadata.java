/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Other properties of the build.
 */
public class Metadata {
    /**
     * Identifies this particular build invocation, which can be useful for finding associated logs or
     * other ad-hoc analysis. The exact meaning and format is defined by builder.id; by default it is
     * treated as opaque and case-sensitive. The value SHOULD be globally unique.
     */
    @JsonAlias({"buildInvocationID"}) // slsa-github-generator wrongly generates json documents with field "buildInvocationID".
    private String buildInvocationId;

    /**
     * The timestamp of when the build started. A point in time, represented as a string in RFC 3339
     * format in the UTC time zone ("Z").
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private OffsetDateTime buildStartedOn;

    /**
     * The timestamp of when the build completed.A point in time, represented as a string in RFC 3339
     * format in the UTC time zone ("Z").
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private OffsetDateTime buildFinishedOn;

    /**
     * Indicates that the builder claims certain fields in this message to be complete.
     */
    private Completeness completeness;

    /**
     * If true, the builder claims that running recipe on materials will produce bit-for-bit identical
     * output.
     */
    private boolean reproducible;

    public String getBuildInvocationId() {
        return buildInvocationId;
    }

    public OffsetDateTime getBuildStartedOn() {
        return buildStartedOn;
    }

    public OffsetDateTime getBuildFinishedOn() {
        return buildFinishedOn;
    }

    public Completeness getCompleteness() {
        return completeness;
    }

    public boolean isReproducible() {
        return reproducible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Metadata other = (Metadata) o;
        return reproducible == other.reproducible                         &&
               Objects.equals(buildInvocationId, other.buildInvocationId) &&
               Objects.equals(buildStartedOn,    other.buildStartedOn)    &&
               Objects.equals(buildFinishedOn,   other.buildFinishedOn)   &&
               Objects.equals(completeness,      other.completeness);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildInvocationId, buildStartedOn, buildFinishedOn, completeness, reproducible);
    }
}
