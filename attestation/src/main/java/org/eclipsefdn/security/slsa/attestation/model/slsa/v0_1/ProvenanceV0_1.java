/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.eclipsefdn.security.slsa.attestation.model.slsa.Provenance;

import java.util.List;
import java.util.Objects;

/**
 * Model class to represent a SLSA Provenance of v0.1.
 *
 * @see <a href="https://slsa.dev/provenance/v0.1">SLSA Provenance v0.1 specification</a>
 */
public class ProvenanceV0_1 extends Provenance {
    public static final String TYPE = "https://slsa.dev/provenance/v0.1";

    @Override
    public String getPredicateType() {
        return TYPE;
    }

    /**
     * Identifies the entity that executed the recipe, which is trusted to have correctly performed
     * the operation and populated this provenance.
     */
    private Builder builder;

    /**
     * Identifies the configuration used for the build. When combined with materials, this SHOULD
     * fully describe the build, such that re-running this recipe results in bit-for-bit identical
     * output (if the build is reproducible).
     * <p>
     * MAY be unset/null if unknown, but this is DISCOURAGED.
     */
    @JsonInclude(Include.NON_NULL)
    private Recipe recipe;

    /**
     * Other properties of the build.
     */
    @JsonInclude(Include.NON_NULL)
    private Metadata metadata;

    /**
     * The collection of artifacts that influenced the build including sources, dependencies, build
     * tools, base images, and so on.
     * <p>
     * This is considered to be incomplete unless metadata.completeness.materials is true. Unset or
     * null is equivalent to empty.
     */
    private List<Material> materials;

    public Builder getBuilder() {
        return builder;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public Iterable<Material> getMaterials() {
        return materials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProvenanceV0_1 other = (ProvenanceV0_1) o;
        return builder.equals(other.builder)             &&
               Objects.equals(recipe,    other.recipe)   &&
               Objects.equals(metadata,  other.metadata) &&
               Objects.equals(materials, other.materials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(builder, recipe, metadata, materials);
    }
}
