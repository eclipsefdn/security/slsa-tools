/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import java.util.Map;
import java.util.Objects;

/**
 * Identifies the configuration used for the build. When combined with materials, this SHOULD fully
 * describe the build, such that re-running this recipe results in bit-for-bit identical output (if
 * the build is reproducible).
 * <p>
 * MAY be unset/null if unknown, but this is DISCOURAGED.
 * <p>
 * NOTE: The Recipe entity has additional properties: arguments and environment that are
 * classified as generic objects in the spec. For this reason it would be expected that builders
 * using this library would extend from the default Recipe and create their own custom class.
 */
public class Recipe {
    /**
     * URI indicating what type of recipe was performed. It determines the meaning of
     * recipe.entryPoint, recipe.arguments, recipe.environment, and materials. (<a
     * href="https://github.com/in-toto/attestation/blob/main/spec/field_types.md#TypeURI">TypeURI</a>)
     */
    private String type;

    /**
     * Index in materials containing the recipe steps that are not implied by recipe.type. For
     * example, if the recipe type were “make”, then this would point to the source containing the
     * Makefile, not the make program itself.
     *
     * <p>Omit this field (or use null) if the recipe doesn't come from a material.
     */
    private Integer definedInMaterial;

    /**
     * String identifying the entry point into the build. This is often a path to a configuration file
     * and/or a target label within that file. The syntax and meaning are defined by recipe.type. For
     * example, if the recipe type were “make”, then this would reference the directory in which to
     * run make as well as which target to use.
     * <p>
     * Consumers SHOULD accept only specific recipe.entryPoint values. For example, a policy might
     * only allow the “release” entry point but not the “debug” entry point.
     * <p>
     * MAY be omitted if the recipe type specifies a default value.
     */
    private String entryPoint;

    private Map<String, Object> arguments;

    private Map<String, Object> environment;

    public String getType() {
        return type;
    }

    public int getDefinedInMaterial() {
        return definedInMaterial;
    }

    public String getEntryPoint() {
        return entryPoint;
    }

    public Map<String, Object> getArguments() {
        return arguments;
    }

    public Map<String, Object> getEnvironment() {
        return environment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Recipe other = (Recipe) o;
        return type.equals(other.type)                                    &&
               Objects.equals(definedInMaterial, other.definedInMaterial) &&
               Objects.equals(entryPoint,        other.entryPoint)        &&
               Objects.equals(arguments,         other.arguments)         &&
               Objects.equals(environment,       other.environment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, definedInMaterial, entryPoint, arguments, environment);
    }
}
