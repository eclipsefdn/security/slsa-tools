/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Encodes whether the builder that created the {@link ProvenanceV0_2 Provenance}
 * claims if certain information contained in the provenance itself is considered
 * to be complete.
 */
public class Completeness {
    private final boolean parametersComplete;
    private final boolean environmentComplete;
    private final boolean materialsComplete;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link Completeness}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private Completeness(@JsonProperty("parameters")  boolean parametersComplete,
                         @JsonProperty("environment") boolean environmentComplete,
                         @JsonProperty("materials")   boolean materialsComplete) {
        this.parametersComplete  = parametersComplete;
        this.environmentComplete = environmentComplete;
        this.materialsComplete   = materialsComplete;
    }

    /**
     * If true, the builder claims that {@code invocation.parameters} is complete,
     * meaning that all external inputs are properly captured in {@code invocation.parameters}.
     */
    public boolean isParametersComplete() {
        return parametersComplete;
    }

    /**
     * If true, the builder claims that {@code invocation.environment} is complete.
     */
    public boolean isEnvironmentComplete() {
        return environmentComplete;
    }

    /**
     * If true, the builder claims that materials is complete, usually through
     * some controls to prevent network access. Sometimes called “hermetic”.
     */
    public boolean isMaterialsComplete() {
        return materialsComplete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Completeness other = (Completeness) o;
        return parametersComplete  == other.parametersComplete  &&
               environmentComplete == other.environmentComplete &&
               materialsComplete   == other.materialsComplete;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parametersComplete, environmentComplete, materialsComplete);
    }

    @Override
    public String toString() {
        return String.format("Completeness[parametersComplete=%s, environmentComplete=%s, materialsComplete=%s]",
                             parametersComplete, environmentComplete, materialsComplete);
    }

    /**
     * A builder class to construct instances of {@link Completeness}.
     */
    public static class Builder {
        private boolean parametersComplete  = false;
        private boolean environmentComplete = false;
        private boolean materialsComplete   = false;

        private Builder() {}

        /**
         * Set the {@code parameters} property to true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteParameters() {
            this.parametersComplete = true;
            return this;
        }

        /**
         * Set the {@code parameters} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteParameters(boolean value) {
            this.parametersComplete = value;
            return this;
        }

        /**
         * Set the {@code environment} property to true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteEnvironment() {
            this.environmentComplete = true;
            return this;
        }

        /**
         * Set the {@code environment} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteEnvironment(boolean value) {
            this.environmentComplete = value;
            return this;
        }

        /**
         * Set the {@code materials} property to true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteMaterials() {
            this.materialsComplete = true;
            return this;
        }

        /**
         * Set the {@code materials} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteMaterials(boolean value) {
            this.materialsComplete = value;
            return this;
        }

        /**
         * Builds a new instance of {@link Completeness} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link Completeness} instance
         */
        public Completeness build() {
            return new Completeness(parametersComplete, environmentComplete, materialsComplete);
        }
    }
}
