/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Describes where the config file that kicked off the build came from.
 * <p>
 * This is effectively a pointer to the source where {@code buildConfig} came from.
 */
public class ConfigSource {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String uri;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final DigestSet digests;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String entryPoint;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link ConfigSource}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private ConfigSource(@JsonProperty("uri")        String uri,
                         @JsonProperty("digest")     DigestSet digests,
                         @JsonProperty("entryPoint") String entryPoint) {
        this.uri        = uri;
        this.digests    = digests;
        this.entryPoint = entryPoint;
    }

    /**
     * Returns a URI indicating the identity of the source of the config.
     * <p>
     * Example: {@code git+https://github.com/someorg/somerepo@refs/heads/main}
     *
     * @see <a href="https://github.com/in-toto/attestation/blob/main/spec/v0.1.0/field_types.md#ResourceURI">ResourceURI</a>
     */
    public String getUri() {
        return uri;
    }

    /**
     * Returns a collection of cryptographic digests for the contents of the artifact specified by {@link #uri}.
     * <p>
     * In case the uri points to a source code repository, the {@link DigestSet} will typically contain the
     * commit hash that represents the state of repository.
     */
    public DigestSet getDigests() {
        return digests;
    }

    /**
     * Returns a string identifying the entry point into the build.
     * <p>
     * This is often a path to a configuration file and/or a target label within that file.
     * The syntax and meaning are defined by {@code buildType}. For example, if the buildType
     * were “make”, then this would reference the directory in which to run make as well as
     * which target to use.
     * <p>
     * Consumers SHOULD accept only specific invocation.entryPoint values. For example, a policy
     * might only allow the “release” entry point but not the “debug” entry point.
     * <p>
     * MAY be omitted if the buildType specifies a default value.
     * <p>
     * Design rationale: The entryPoint is distinct from parameters to make it easier to write
     * secure policies without having to parse parameters.
     */
    public String getEntryPoint() {
        return entryPoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConfigSource other = (ConfigSource) o;
        return Objects.equals(uri,        other.uri)     &&
               Objects.equals(digests,    other.digests) &&
               Objects.equals(entryPoint, other.entryPoint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, digests, entryPoint);
    }

    @Override
    public String toString() {
        return String.format("ConfigSource[uri='%s', digests=%s, entryPoint='%s']", uri, digests, entryPoint);
    }

    /**
     * A builder class to construct instances of {@link ConfigSource}.
     */
    public static class Builder {
        private String    uri;
        private DigestSet digests = DigestSet.empty();
        private String    entryPoint;

        private Builder() {}

        /**
         * Set the {@code uri} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withUri(@Nonnull String uri) {
            Objects.requireNonNull(uri);
            this.uri = uri;
            return this;
        }

        /**
         * Set the {@code digests} property to the given value.
         * <p>
         * Note: this method will override any digest that was added
         * previously using {@code #withDigest(DigestSetAlgorithmType, String)}.
         *
         * @return this {@link Builder} instance
         */
        public Builder withDigests(@Nonnull DigestSet digests) {
            Objects.requireNonNull(digests);
            this.digests = digests;
            return this;
        }

        /**
         * Adds another digest entry to the existing digests.
         *
         * @return this {@link Builder} instance
         */
        public Builder withDigest(@Nonnull DigestAlgorithm algorithmType, @Nonnull String hash) {
            Objects.requireNonNull(algorithmType);
            Objects.requireNonNull(hash);
            this.digests.addDigest(algorithmType.getValue(), hash);
            return this;
        }

        /**
         * Set the {@code entryPoint} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withEntryPoint(@Nonnull String entryPoint) {
            Objects.requireNonNull(entryPoint);
            this.entryPoint = entryPoint;
            return this;
        }

        /**
         * Builds a new instance of {@link ConfigSource} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link ConfigSource} instance
         */
        public ConfigSource build() {
            return new ConfigSource(uri, digests, entryPoint);
        }
    }
}
