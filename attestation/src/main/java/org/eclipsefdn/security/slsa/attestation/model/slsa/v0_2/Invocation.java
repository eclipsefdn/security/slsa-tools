/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Identifies the configuration used for the build. When combined with materials, this SHOULD fully
 * describe the build, such that re-running this recipe results in bit-for-bit identical output (if
 * the build is reproducible).
 * <p>
 * MAY be unset/null if unknown, but this is DISCOURAGED.
 * <p>
 * NOTE: The Recipe entity has additional properties: arguments and environment that are
 * classified as generic objects in the spec. For this reason it would be expected that builders
 * using this library would extend from the default Recipe and create their own custom class.
 */
public class Invocation {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private ConfigSource configSource;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> parameters;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> environment;

    /**
     * Returns a new {@code Builder} instance to build an instance of {@code ConfigSource}.
     *
     * @return a new {@code Builder} instance
     */
    public static Invocation.Builder builder() {
        return new Builder();
    }

    private Invocation() {
        parameters  = new HashMap<>();
        environment = new HashMap<>();
    }

    private Invocation(ConfigSource        configSource,
                       Map<String, Object> parameters,
                       Map<String, Object> environment) {
        this.configSource = configSource;
        this.parameters   = parameters;
        this.environment  = environment;
    }

    /**
     * Describes where the config file that kicked off the build came from.
     * This is effectively a pointer to the source where {@code buildConfig} came from.
     */
    public ConfigSource getConfigSource() {
        return configSource;
    }

    /**
     * Collection of all external inputs that influenced the build on top of {@link #getConfigSource()}.
     * <p>
     * For example, if the invocation type were “make”, then this might be the flags passed to make
     * aside from the target, which is captured in {@code invocation.configSource.entryPoint}.
     * <p>
     * Consumers SHOULD accept only “safe” {@link #getParameters()}. The simplest and safest way to
     * achieve this is to disallow any parameters altogether.
     * <p>
     * This is an arbitrary JSON object with a schema defined by buildType.
     * <p>
     * This is considered to be incomplete unless {@code metadata.completeness.parameters} is true.
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setParameters(Map<String, Object> parameters) {
        Objects.requireNonNull(parameters);
        this.parameters = parameters;
    }

    /**
     * Any other builder-controlled inputs necessary for correctly evaluating the build.
     * Usually only needed for reproducing the build but not evaluated as part of policy.
     * <p>
     * This SHOULD be minimized to only include things that are part of the public API,
     * that cannot be recomputed from other values in the provenance, and that actually
     * affect the evaluation of the build. For example, this might include variables that
     * are referenced in the workflow definition, but it SHOULD NOT include a dump of all
     * environment variables or include things like the hostname (assuming hostname is not
     * part of the public API).
     * <p>
     * This is an arbitrary JSON object with a schema defined by buildType.
     * <p>
     * This is considered to be incomplete unless {@code metadata.completeness.environment} is true.
     */
    public Map<String, Object> getEnvironment() {
        return environment;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setEnvironment(Map<String, Object> environment) {
        Objects.requireNonNull(environment);
        this.environment = environment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invocation other = (Invocation) o;
        return Objects.equals(configSource, other.configSource) &&
               Objects.equals(parameters,   other.parameters)   &&
               Objects.equals(environment,  other.environment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configSource, parameters, environment);
    }

    @Override
    public String toString() {
        return String.format("Invocation[configSource=%s, parameters=%s, environment=%s]", configSource, parameters, environment);
    }

    /**
     * A builder class to construct instances of {@link Invocation}.
     */
    public static class Builder {
        private ConfigSource        configSource;
        private Map<String, Object> parameters  = new HashMap<>();
        private Map<String, Object> environment = new HashMap<>();

        private Builder() {}

        /**
         * Set the {@code configSource} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withConfigSource(@Nonnull ConfigSource configSource) {
            Objects.requireNonNull(configSource);
            this.configSource = configSource;
            return this;
        }

        /**
         * Set the {@code parameters} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withParameters(@Nonnull Map<String, Object> parameters) {
            Objects.requireNonNull(parameters);
            this.parameters = parameters;
            return this;
        }

        /**
         * Set the {@code environment} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withEnvironment(@Nonnull Map<String, Object> environment) {
            Objects.requireNonNull(environment);
            this.environment = environment;
            return this;
        }

        /**
         * Builds a new instance of {@link Invocation} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link Invocation} instance
         */
        public Invocation build() {
            return new Invocation(configSource, parameters, environment);
        }
    }
}
