/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * The collection of artifacts that influenced the build including sources, dependencies, build
 * tools, base images, and so on.
 * <p>
 * This is considered to be incomplete unless {@code metadata.completeness.materials} is true.
 * Unset or null is equivalent to empty.
 */
public class Material {
    private String uri;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private DigestSet digests;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link Material}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private Material(@JsonProperty("uri")    String uri,
                     @JsonProperty("digest") DigestSet digests) {
        this.uri = uri;
        this.digests = digests;
    }

    /**
     * Returns the method by which this artifact was referenced during the build.
     *
     * @see <a href="https://github.com/in-toto/attestation/blob/main/spec/field_types.md#ResourceURI">ResourceURI</a>
     */
    public String getUri() {
        return uri;
    }

    /**
     * Returns a collection of cryptographic digests for the contents of this artifact.
     */
    public DigestSet getDigests() {
        return digests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Material other = (Material) o;
        return Objects.equals(uri,     other.uri) &&
               Objects.equals(digests, other.digests);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, digests);
    }

    @Override
    public String toString() {
        return String.format("Material[uri='%s', digest=%s]", uri, digests);
    }

    /**
     * A builder class to construct instances of {@link Material}.
     */
    public static class Builder {
        private String    uri;
        private DigestSet digests = DigestSet.empty();

        private Builder() {}

        /**
         * Set the {@code uri} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withUri(@Nonnull String uri) {
            Objects.requireNonNull(uri);
            this.uri = uri;
            return this;
        }

        /**
         * Set the {@code digests} property to the given value.
         * <p>
         * Note: this method will override any digest that was added
         * previously using {@code #withDigest(DigestSetAlgorithmType, String)}.
         *
         * @return this {@link Builder} instance
         */
        public Builder withDigests(@Nonnull DigestSet digests) {
            Objects.requireNonNull(digests);
            this.digests = digests;
            return this;
        }

        /**
         * Adds another digest entry to the existing digests.
         *
         * @return this {@link Builder} instance
         */
        public Builder withDigest(@Nonnull DigestAlgorithm algorithmType, @Nonnull String hash) {
            Objects.requireNonNull(algorithmType);
            Objects.requireNonNull(hash);
            this.digests.addDigest(algorithmType.getValue(), hash);
            return this;
        }

        /**
         * Builds a new instance of {@link Material} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link Material} instance
         */
        public Material build() {
            return new Material(uri, digests);
        }
    }
}
