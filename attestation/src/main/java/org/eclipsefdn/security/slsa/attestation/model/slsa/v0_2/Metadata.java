/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Nonnull;
import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Other properties of the build.
 */
public class Metadata {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String buildInvocationId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private OffsetDateTime buildStartedOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private OffsetDateTime buildFinishedOn;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Completeness completeness;

    private boolean reproducible;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link Metadata}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    @JsonCreator
    private Metadata(@JsonProperty("buildInvocationId") String         buildInvocationId,
                     @JsonProperty("buildStartedOn")    OffsetDateTime buildStartedOn,
                     @JsonProperty("buildFinishedOn")   OffsetDateTime buildFinishedOn,
                     @JsonProperty("completeness")      Completeness   completeness,
                     @JsonProperty("reproducible")      boolean        reproducible) {
        this.buildInvocationId = buildInvocationId;
        this.buildStartedOn    = buildStartedOn;
        this.buildFinishedOn   = buildFinishedOn;
        this.completeness      = completeness;
        this.reproducible      = reproducible;
    }

    /**
     * Identifies this particular build invocation, which can be useful for finding
     * associated logs or other ad-hoc analysis.
     * <p>
     * The exact meaning and format is defined by {@code builder.id}; by default it is
     * treated as opaque and case-sensitive. The value SHOULD be globally unique.
     */
    public String getBuildInvocationId() {
        return buildInvocationId;
    }

    /**
     * Returns the timestamp when the build started.
     * <p>
     * A point in time, represented as a string in RFC 3339 format in the UTC time zone ("Z").
     */
    public OffsetDateTime getBuildStartedOn() {
        return buildStartedOn;
    }

    /**
     * Returns the timestamp when the build completed.
     * <p>
     * A point in time, represented as a string in RFC 3339 format in the UTC time zone ("Z").
     */
    public OffsetDateTime getBuildFinishedOn() {
        return buildFinishedOn;
    }

    /**
     * Indicates that the builder claims certain fields in this message to be complete.
     */
    public Completeness getCompleteness() {
        return completeness;
    }

    /**
     * If true, the builder claims that running recipe on materials will produce
     * bit-for-bit identical output.
     */
    public boolean isReproducible() {
        return reproducible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Metadata other = (Metadata) o;
        return Objects.equals(buildInvocationId, other.buildInvocationId) &&
               Objects.equals(buildStartedOn,    other.buildStartedOn)    &&
               Objects.equals(buildFinishedOn,   other.buildFinishedOn)   &&
               Objects.equals(completeness,      other.completeness)      &&
               reproducible == other.reproducible;
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildInvocationId, buildStartedOn, buildFinishedOn, completeness, reproducible);
    }

    @Override
    public String toString() {
        return String.format("Metadata[buildInvocationId=%s,buildStartedOn=%s,buildFinishedOn=%s,completeness=%s,reproducible=%s]",
                             buildInvocationId, buildStartedOn, buildFinishedOn, completeness, reproducible);
    }

    /**
     * A builder class to construct instances of {@link Metadata}.
     */
    public static class Builder {
        private String         buildInvocationId;
        private OffsetDateTime buildStartedOn;
        private OffsetDateTime buildFinishedOn;
        private Completeness   completeness;
        private boolean        reproducible;

        private Builder() {}

        /**
         * Set the {@code buildInvocationId} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuildInvocationId(@Nonnull String buildInvocationId) {
            Objects.requireNonNull(buildInvocationId);
            this.buildInvocationId = buildInvocationId;
            return this;
        }

        /**
         * Set the {@code buildStartedOn} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuildStartedOn(@Nonnull OffsetDateTime buildStartedOn) {
            Objects.requireNonNull(buildStartedOn);
            this.buildStartedOn = buildStartedOn;
            return this;
        }

        /**
         * Set the {@code buildFinishedOn} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuildFinishedOn(@Nonnull OffsetDateTime buildFinishedOn) {
            Objects.requireNonNull(buildFinishedOn);
            this.buildFinishedOn = buildFinishedOn;
            return this;
        }

        /**
         * Set the {@code completeness} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withCompleteness(@Nonnull Completeness completeness) {
            Objects.requireNonNull(completeness);
            this.completeness = completeness;
            return this;
        }

        /**
         * Set the {@code reproducible} property to the true.
         *
         * @return this {@link Builder} instance
         */
        public Builder withReproducible() {
            this.reproducible = true;
            return this;
        }

        /**
         * Set the {@code reproducible} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withReproducible(boolean reproducible) {
            this.reproducible = reproducible;
            return this;
        }

        /**
         * Builds a new instance of {@link Metadata} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link Metadata} instance
         */
        public Metadata build() {
            return new Metadata(buildInvocationId,
                                buildStartedOn,
                                buildFinishedOn,
                                completeness,
                                reproducible);
        }
    }
}
