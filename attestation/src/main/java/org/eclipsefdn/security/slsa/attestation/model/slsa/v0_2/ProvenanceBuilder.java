/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Identifies the entity that executed the recipe, which is trusted to have correctly performed the
 * operation and populated this provenance.
 * <p>
 * The identity MUST reflect the trust base that consumers care about. How detailed to be is a
 * judgement call. For example, GitHub Actions supports both GitHub-hosted runners and self-hosted
 * runners. The GitHub-hosted runner might be a single identity because, it’s all GitHub from the
 * consumer’s perspective. Meanwhile, each self-hosted runner might have its own identity because
 * not all runners are trusted by all consumers.
 * <p>
 * Consumers MUST accept only specific (signer, builder) pairs. For example, the “GitHub” can
 * sign provenance for the “GitHub Actions” builder, and “Google” can sign provenance for the
 * “Google Cloud Build” builder, but “GitHub” cannot sign for the “Google Cloud Build” builder.
 * <p>
 * Design rationale: The builder is distinct from the signer because one signer may generate
 * attestations for more than one builder, as in the GitHub Actions example above. The field is
 * required, even if it is implicit from the signer, to aid readability and debugging. It is an
 * object to allow additional fields in the future, in case one URI is not sufficient.
 */
public class ProvenanceBuilder {
    private final String id;

    /**
     * Returns a new {@link ProvenanceBuilder} instance with the given {@code id}.
     *
     * @param id the id of the {@link ProvenanceBuilder}
     * @return a new {@code ProvenanceBuilder} instance
     */
    public static ProvenanceBuilder of(@Nonnull String id) {
        Objects.requireNonNull(id);
        return new ProvenanceBuilder(id);
    }

    @JsonCreator
    private ProvenanceBuilder(@JsonProperty("id") String id) {
        this.id = id;
    }

    /**
     * Returns a URI indicating the builder’s identity.
     *
     * @see <a href="https://github.com/in-toto/attestation/blob/main/spec/v0.1.0/field_types.md#TypeURI">TypeURI</a>
     * @return a URI indicating the builder's identity
     */
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProvenanceBuilder other = (ProvenanceBuilder) o;
        return id.equals(other.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.format("ProvenanceBuilder[id='%s']", id);
    }
}
