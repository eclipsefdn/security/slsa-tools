/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import org.eclipsefdn.security.slsa.attestation.model.slsa.Provenance;

import javax.annotation.Nonnull;
import java.util.*;

/**
 * Provenance is an attestation that some entity ({@code builder}) produced one or more software artifacts
 * (the subject of an in-toto attestation {@code Statement}) by executing some invocation, using some
 * other artifacts as input (materials).
 * <p>
 * The invocation in turn runs the {@code buildConfig}, which is a record of what was executed.
 * The builder is trusted to have faithfully recorded the provenance; there is no option but to trust the builder.
 * However, the builder may have performed this operation at the request of some external, possibly untrusted entity.
 * These untrusted parameters are captured in the invocation’s parameters and some of the materials.
 * <p>
 * Finally, the build may have depended on various environmental parameters (environment) that are needed for reproducing
 * the build but that are not under external control.
 *
 * @see <a href="https://slsa.dev/provenance/v0.2">SLSA Provenance v0.2 specification</a>
 */
public class ProvenanceV0_2 extends Provenance {
    public static final String TYPE = "https://slsa.dev/provenance/v0.2";

    @Override
    public String getPredicateType() {
        return TYPE;
    }

    @JsonInclude(Include.NON_NULL)
    private String buildType;

    @JsonInclude(Include.NON_NULL)
    private ProvenanceBuilder builder;

    @JsonInclude(Include.NON_NULL)
    private Invocation invocation;

    @JsonInclude(Include.NON_EMPTY)
    private Map<String, Object> buildConfig;

    @JsonInclude(Include.NON_NULL)
    private Metadata metadata;

    @JsonInclude(Include.NON_EMPTY)
    private List<Material> materials;

    /**
     * Returns a new {@link Builder} instance to build an instance of {@link ProvenanceV0_2}.
     *
     * @return a new {@link Builder} instance
     */
    public static Builder builder() {
        return new Builder();
    }

    private ProvenanceV0_2() {
        this.buildConfig = new HashMap<>();
        this.materials   = new ArrayList<>();
    }

    private ProvenanceV0_2(String              buildType,
                           ProvenanceBuilder   builder,
                           Invocation          invocation,
                           Map<String, Object> buildConfig,
                           Metadata            metadata,
                           List<Material>      materials) {
        this.buildType   = buildType;
        this.builder     = builder;
        this.invocation  = invocation;
        this.buildConfig = buildConfig;
        this.metadata    = metadata;
        this.materials   = materials;
    }

    /**
     * A URI indicating what type of build was performed.
     * It determines the meaning of invocation, buildConfig and materials.
     */
    public String getBuildType() {
        return buildType;
    }

    /**
     * Identifies the entity that executed the recipe, which is trusted to
     * have correctly performed the operation and populated this provenance.
     */
    public ProvenanceBuilder getBuilder() {
        return builder;
    }

    /**
     * Identifies the event that kicked off the build.
     * <p>
     * When combined with materials, this SHOULD fully describe the build,
     * such that re-running this invocation results in bit-for-bit identical
     * output (if the build is reproducible).
     * <p>
     * MAY be unset/null if unknown, but this is DISCOURAGED.
     */
    public Invocation getInvocation() {
        return invocation;
    }

    /**
     * Lists the steps in the build.
     * <p>
     * If {@code invocation.configSource} is not available, {@code buildConfig} can be
     * used to verify information about the build.
     * <p>
     * This is an arbitrary JSON object with a schema defined by {@link #buildType}.
     */
    public Map<String, Object> getBuildConfig() {
        return buildConfig;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setBuildConfig(Map<String, Object> buildConfig) {
        Objects.requireNonNull(buildConfig);
        this.buildConfig = buildConfig;
    }

    /**
     * Other properties of the build.
     */
    public Metadata getMetadata() {
        return metadata;
    }

    /**
     * The collection of artifacts that influenced the build including sources, dependencies, build
     * tools, base images, and so on.
     * <p>
     * This is considered to be incomplete unless metadata.completeness.materials is true. Unset or
     * null is equivalent to empty.
     */
    public Collection<Material> getMaterials() {
        return materials;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setMaterials(List<Material> materials) {
        Objects.requireNonNull(materials);
        this.materials = materials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProvenanceV0_2 other = (ProvenanceV0_2) o;
        return buildType.equals(other.buildType)              &&
               builder.equals(other.builder)                  &&
               Objects.equals(invocation,  other.invocation)  &&
               Objects.equals(buildConfig, other.buildConfig) &&
               Objects.equals(metadata,    other.metadata)    &&
               Objects.equals(materials,   other.materials);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildType, builder, invocation, buildConfig, metadata, materials);
    }

    @Override
    public String toString() {
        return String.format("Provenance[buildType=%s, builder=%s, invocation=%s, buildConfig=%s, metadata=%s, materials=%s]",
                             buildType, builder, invocation, buildConfig, metadata, materials);
    }

    /**
     * A builder class to construct instances of {@link ProvenanceV0_2}.
     */
    public static class Builder {
        private String              buildType;
        private ProvenanceBuilder   builder;
        private Invocation          invocation;
        private Map<String, Object> buildConfig = new HashMap<>();
        private Metadata            metadata;
        private List<Material>      materials   = new ArrayList<>();

        private Builder() {}

        /**
         * Set the {@code buildType} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuildType(@Nonnull String buildType) {
            Objects.requireNonNull(buildType);
            this.buildType = buildType;
            return this;
        }

        /**
         * Set the {@code builder} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuilder(@Nonnull ProvenanceBuilder builder) {
            Objects.requireNonNull(builder);
            this.builder = builder;
            return this;
        }

        /**
         * Set the {@code invocation} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withInvocation(@Nonnull Invocation invocation) {
            Objects.requireNonNull(invocation);
            this.invocation = invocation;
            return this;
        }

        /**
         * Set the {@code buildConfig} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withBuildConfig(@Nonnull Map<String, Object> buildConfig) {
            Objects.requireNonNull(buildConfig);
            this.buildConfig = buildConfig;
            return this;
        }

        /**
         * Set the {@code metadata} property to the given value.
         *
         * @return this {@link Builder} instance
         */
        public Builder withMetadata(@Nonnull Metadata metadata) {
            Objects.requireNonNull(metadata);
            this.metadata = metadata;
            return this;
        }

        /**
         * Add a {@link Material} object to the {@code materials}.
         *
         * @return this {@link Builder} instance
         */
        public Builder withMaterial(@Nonnull Material material) {
            Objects.requireNonNull(material);
            this.materials.add(material);
            return this;
        }

        /**
         * Builds a new instance of {@link ProvenanceV0_2} with property values
         * captured by this {@link Builder}.
         *
         * @return a new {@link ProvenanceV0_2} instance
         */
        public ProvenanceV0_2 build() {
            return new ProvenanceV0_2(buildType,
                                      builder,
                                      invocation,
                                      buildConfig,
                                      metadata,
                                      materials);
        }
    }
}
