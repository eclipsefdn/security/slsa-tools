/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;

import java.util.Objects;

public class ArtifactReference {
    private String uri;

    @JsonProperty("digest")
    private DigestSet digests;

    private String localName;

    private String downloadLocation;

    private String mediaType;

    public String getUri() {
        return uri;
    }

    public DigestSet getDigests() {
        return digests;
    }

    public String getLocalName() {
        return localName;
    }

    public String getDownloadLocation() {
        return downloadLocation;
    }

    public String getMediaType() {
        return mediaType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ArtifactReference other = (ArtifactReference) o;
        return uri.equals(other.uri)                                     &&
               Objects.equals(digests,          other.digests)          &&
               Objects.equals(localName,        other.localName)        &&
               Objects.equals(downloadLocation, other.downloadLocation) &&
               Objects.equals(mediaType,        other.mediaType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, digests, localName, downloadLocation, mediaType);
    }

    @Override
    public String toString() {
        return
            String.format("ArtifactReference[uri=%s,digests=%s,localName=%s,downloadLocation=%s,mediaType=%s]",
                          uri,
                          digests,
                          localName,
                          downloadLocation,
                          mediaType);
    }
}
