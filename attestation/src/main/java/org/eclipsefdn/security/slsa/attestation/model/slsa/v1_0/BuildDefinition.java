/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * The BuildDefinition describes all the inputs to the build.
 * It SHOULD contain all the information necessary and sufficient
 * to initialize the build and begin execution.
 * <p>
 * The externalParameters and systemParameters are the top-level inputs to the template,
 * meaning inputs not derived from another input. Each is an arbitrary JSON object, though it is RECOMMENDED to keep the structure simple with string values to aid verification. The same field name SHOULD NOT be used for both externalParameters and systemParameters.
 *
 * The parameters SHOULD only contain the actual values passed in through the interface to the build system. Metadata about those parameter values, particularly digests of artifacts referenced by those parameters, SHOULD instead go in resolvedDependencies. The documentation for buildType SHOULD explain how to convert from a parameter to the dependency uri.
 */
public class BuildDefinition {
    /**
     * Identifies the template for how to perform the build and interpret the parameters and dependencies.
     * <p>
     * The URI SHOULD resolve to a human-readable specification that includes:
     * <ul>
     *     <li>overall description of the build type</li>
     *     <li>schema for externalParameters and systemParameters</li>
     *     <li>unambiguous instructions for how to initiate the build given this BuildDefinition</li>
     *     <li>a complete example. Example: <a href="https://slsa.dev/github-actions-workflow/v1">github actions workflow</a></li>
     * </ul>
     */
    private String buildType;

    /**
     * The parameters that are under external control, such as those set by a user or tenant of the build system.
     * They MUST be complete at SLSA Build L3, meaning that there is no additional mechanism for an external
     * party to influence the build. (At lower SLSA Build levels, the completeness MAY be best effort.)
     * <p>
     * The build system SHOULD be designed to minimize the size and complexity of {@code externalParameters},
     * in order to reduce fragility and ease verification. Consumers SHOULD have an expectation of what
     * “good” looks like; the more information that they need to check, the harder that task becomes.
     * <p>
     * Verifiers SHOULD reject unrecognized or unexpected fields within {@code externalParameters}.
     */
    private Map<String, Object> externalParameters;

    /**
     * The parameters that are under the control of the entity represented by builder.id.
     * The primary intention of this field is for debugging, incident response, and vulnerability
     * management. The values here MAY be necessary for reproducing the build.
     * There is no need to verify these parameters because the build system is already trusted,
     * and in many cases it is not practical to do so.
     */
    private Map<String, Object> systemParameters;

    private List<ArtifactReference> resolvedDependencies;

    public String getBuildType() {
        return buildType;
    }

    public Map<String, Object> getExternalParameters() {
        return externalParameters;
    }

    public Map<String, Object> getSystemParameters() {
        return systemParameters;
    }

    public List<ArtifactReference> getResolvedDependencies() {
        return resolvedDependencies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BuildDefinition other = (BuildDefinition) o;
        return buildType.equals(other.buildType)                                &&
               Objects.equals(externalParameters,   other.externalParameters)   &&
               Objects.equals(systemParameters,     other.systemParameters)     &&
               Objects.equals(resolvedDependencies, other.resolvedDependencies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildType, externalParameters, systemParameters, resolvedDependencies);
    }

    @Override
    public String toString() {
        return
            String.format("BuildDefinition[buildType=%s,externalParameters=%d,systemParameters=%d,resolvedDependencies=%d]",
                          buildType,
                          externalParameters.size(),
                          systemParameters.size(),
                          resolvedDependencies.size());
    }
}
