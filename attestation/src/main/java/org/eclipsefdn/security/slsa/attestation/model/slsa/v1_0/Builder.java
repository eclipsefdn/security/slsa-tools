/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class Builder {
    private String id;

    private String version;

    private List<ArtifactReference> builderDependencies = new ArrayList<>();

    @Nonnull
    public String getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }

    @Nonnull
    public Collection<ArtifactReference> getBuilderDependencies() {
        return builderDependencies;
    }

    @JsonSetter(nulls = Nulls.AS_EMPTY)
    private void setBuilderDependencies(@Nonnull List<ArtifactReference> references) {
        Objects.requireNonNull(references);
        this.builderDependencies = references;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Builder other = (Builder) o;
        return id.equals(other.id)           &&
               version.equals(other.version) &&
               Objects.equals(builderDependencies, other.builderDependencies);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, version, builderDependencies);
    }

    @Override
    public String toString() {
        return String.format("Builder[id=%s,version=%s,builderDependencies=%s]", id, version, builderDependencies);
    }
}
