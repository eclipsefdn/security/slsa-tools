/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.OffsetDateTime;
import java.util.Objects;

/**
 * Other properties of the build.
 */
public class Metadata {
    private String invocationId;

    /**
     * The timestamp of when the build started. A point in time, represented as a string in RFC 3339
     * format in the UTC time zone ("Z").
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private OffsetDateTime startedOn;

    /**
     * The timestamp of when the build completed.A point in time, represented as a string in RFC 3339
     * format in the UTC time zone ("Z").
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX")
    private OffsetDateTime finishedOn;

    public String getInvocationId() {
        return invocationId;
    }

    public OffsetDateTime getStartedOn() {
        return startedOn;
    }

    public OffsetDateTime getFinishedOn() {
        return finishedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Metadata other = (Metadata) o;
        return invocationId.equals(other.invocationId)    &&
               Objects.equals(startedOn, other.startedOn) &&
               Objects.equals(finishedOn, other.finishedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invocationId, startedOn, finishedOn);
    }
}
