/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import org.eclipsefdn.security.slsa.attestation.model.slsa.Provenance;

import java.util.Objects;

/**
 * Model class to represent a SLSA Provenance v1.
 *
 * @see <a href="https://slsa.dev/provenance/v1">SLSA Provenance v1 specification</a>
 */
public class ProvenanceV1 extends Provenance {
    public static final String TYPE = "https://slsa.dev/provenance/v1";

    @Override
    public String getPredicateType() {
        return TYPE;
    }

    private BuildDefinition buildDefinition;

    private RunDetails runDetails;

    public BuildDefinition getBuildDefinition() {
        return buildDefinition;
    }

    public RunDetails getRunDetails() {
        return runDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProvenanceV1 other = (ProvenanceV1) o;
        return buildDefinition.equals(other.buildDefinition) &&
               runDetails.equals(other.runDetails);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buildDefinition, runDetails);
    }
}
