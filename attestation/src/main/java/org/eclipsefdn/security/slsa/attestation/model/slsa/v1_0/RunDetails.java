/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v1_0;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

public class RunDetails {
    private Builder builder;

    private Metadata metadata;

    @JsonProperty("byproducts")
    private List<ArtifactReference> byProducts;

    public Builder getBuilder() {
        return builder;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public Iterable<ArtifactReference> getByProducts() {
        return byProducts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RunDetails other = (RunDetails) o;
        return builder.equals(other.builder)               &&
                Objects.equals(metadata,   other.metadata) &&
                Objects.equals(byProducts, other.byProducts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(builder, metadata, byProducts);
    }

    @Override
    public String toString() {
        return
            String.format("RunDetails[builder=%s,metadata=%s,byProducts=%s]",
                          builder,
                          metadata,
                          byProducts);
    }
}
