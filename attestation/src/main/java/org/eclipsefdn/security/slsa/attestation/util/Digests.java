/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.util;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Digests {
    private Digests() {}

    public static String getSHA256Digest(Path path) {
        return getDigest("SHA-256", path);
    }

    public static String getDigest(String algorithm, Path path) {
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            try (InputStream is = new DigestInputStream(Files.newInputStream(path), digest)) {
                int bufferSize = 8096;
                byte[] buffer = new byte[bufferSize];
                while (is.read(buffer, 0, bufferSize) > 0);
            }
            return String.format("%032x", new BigInteger(1, digest.digest()));
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(String.format("unsupported digest algorithm '%s'", algorithm));
        } catch (IOException ioe) {
            throw new RuntimeException(String.format("failed to calculate digest for file '%s'", path));
        }
    }
}
