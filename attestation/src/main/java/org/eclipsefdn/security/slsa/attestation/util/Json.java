/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.eclipsefdn.security.slsa.attestation.model.slsa.SlsaModule;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;

/**
 * A utility class to operate with json data.
 */
public class Json {

    /** The singleton {@link JsonMapper} instance to be used for serialization / deserialization. */
    public static final JsonMapper MAPPER =
            JsonMapper.builder()
                      .addModule(new JavaTimeModule())
                      .addModule(new SlsaModule())
                      .configure(MapperFeature.AUTO_DETECT_CREATORS, false)
                      .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
                      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                      .configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false)
                      .configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
                      .build();

    /** Hide constructor. */
    private Json() {}

    /**
     * Reads data from the given {@code jsonString} and deserializes the json content
     * into an instance of the given {@code entryClass}.
     *
     * @param jsonString the {@link String} to read the json data from
     * @param entryClass the {@link Class} that should be used to deserialize into
     * @return the deserialized json object
     */
    public static <T> T readObject(String jsonString, Class<T> entryClass) throws JsonProcessingException {
        return MAPPER.readerFor(entryClass).readValue(jsonString);
    }

    /**
     * Reads data from the given {@code bytes} and deserializes the json content
     * into an instance of the given {@code entryClass}.
     *
     * @param bytes the byte array to read the json data from
     * @param entryClass the {@link Class} that should be used to deserialize into
     * @return the deserialized json object
     */
    public static <T> T readObject(byte[] bytes, Class<T> entryClass) throws IOException {
        return MAPPER.readerFor(entryClass).readValue(bytes);
    }

    /**
     * Reads data from the given {@code inputStream} and deserializes the json content
     * into instances of the given {@code entryClass}.
     *
     * @param inputStream the {@link InputStream} to read the json data from
     * @param entryClass the {@link Class} that should be used to deserialize into
     * @return a {@link List} of deserialized json objects
     */
    public static <T> List<T> readObjects(InputStream inputStream, Class<T> entryClass) throws IOException {
        try (MappingIterator<T> it = MAPPER.readerFor(entryClass).readValues(inputStream)) {
            return it.readAll();
        }
    }

    /**
     * Serializes and writes the given {@code value} to the given {@code outputStream}.
     *
     * @param outputStream the {@link OutputStream} to write the json data to
     * @param value the object that should be serialized and written
     */
    public static <T> void writeObject(OutputStream outputStream, T value) throws IOException {
        MAPPER.writer()
              .writeValue(outputStream, value);
    }

    /**
     * Serializes and writes the given {@code values} to the given {@code outputStream}.
     * <p>
     * The objects will be written according to the <a href="https://jsonlines.org/">JSON lines standard</a>
     * where each json object is encoded in a single line.
     *
     * @param outputStream the {@link OutputStream} to write the json data to
     * @param values the objects that should be serialized and written
     */
    public static <T> void writeObjects(OutputStream outputStream, Collection<T> values) throws IOException {
        try (SequenceWriter seq = MAPPER.writer()
                                        .withRootValueSeparator("\n")
                                        .writeValues(outputStream)) {
            seq.writeAll(values);
        }
    }

    public static <T> String dumpWithPrettyPrinting(T value) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            JsonGenerator jsonGenerator = MAPPER.writer().createGenerator(baos).useDefaultPrettyPrinter();
            jsonGenerator.writeObject(value);
            return baos.toString(StandardCharsets.UTF_8.name());
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }
}
