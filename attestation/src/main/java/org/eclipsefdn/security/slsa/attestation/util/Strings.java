/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.util;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strings {
    private static final Pattern SEMVER_PATTERN = Pattern.compile("v(\\d+)\\.(\\d+)\\.(\\d+)");

    private Strings() {}

    public static String removePrefix(String input, String prefix) {
        Objects.requireNonNull(input);
        Objects.requireNonNull(prefix);

        if (input.startsWith(prefix)) {
            return input.substring(prefix.length());
        }
        return input;
    }

    public static String removeSuffix(String input, String suffix) {
        Objects.requireNonNull(input);
        Objects.requireNonNull(suffix);

        if (input.endsWith(suffix)) {
            return input.substring(0, input.length() - suffix.length());
        }
        return input;
    }

    public static String shorten(String input) {
        if (input == null) {
            return "<empty>";
        } else if (input.length() < 5) {
            return input;
        } else {
            return input.substring(0, 5) + "...";
        }
    }

    /**
     * Checks whether the given {@code input} is a valid semantic version
     * string in the format {@code vX.Y.Z}.
     *
     * @param input the input to check
     * @return true if it is a valid semantic version string, false otherwise
     */
    public static boolean isValidSemVer(String input) {
        if (input == null || input.isEmpty()) {
            return false;
        }

        Matcher matcher = SEMVER_PATTERN.matcher(input);
        return matcher.matches();
    }
}
