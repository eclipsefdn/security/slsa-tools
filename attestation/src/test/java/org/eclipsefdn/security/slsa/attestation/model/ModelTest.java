/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model;

import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public abstract class ModelTest<T> {

    public abstract T getModelFixture();

    @Test
    public void readWrite() {
        T fixture = getModelFixture();

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Json.writeObject(baos, fixture);

            T result = (T) Json.readObject(baos.toByteArray(), fixture.getClass());
            assertEquals(fixture, result);
        } catch (IOException ex) {
            fail("unexpected exception", ex);
        }
    }
}
