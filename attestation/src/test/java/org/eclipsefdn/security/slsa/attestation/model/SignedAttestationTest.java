/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model;

import org.eclipsefdn.security.slsa.attestation.error.Errors;
import org.eclipsefdn.security.slsa.attestation.error.InvalidModelException;
import org.eclipsefdn.security.slsa.attestation.io.AttestationReader;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

public class SignedAttestationTest {

    private final AttestationReader attestationReader = new AttestationReader();

    @Test
    public void invalidPayloadType() {
        InvalidModelException exception = assertThrows(InvalidModelException.class, () -> {
            try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("envelope/invalid-payload-type.jsonl")) {
                attestationReader.readAttestations(inputStream);
                fail("expected InvalidModelException");
            }
        });

        assertEquals(String.format(Errors.INVALID_DSSE_PAYLOAD, "something/else"), exception.getMessage());
    }

    @Test
    public void invalidContent() {
        InvalidModelException exception = assertThrows(InvalidModelException.class, () -> {
            try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("envelope/invalid-content.jsonl")) {
                attestationReader.readAttestations(inputStream);
                fail("expected InvalidModelException");
            }
        });

        assertEquals(Errors.INVALID_DSSE_ENVELOPE_FORMAT, exception.getMessage());
    }
}
