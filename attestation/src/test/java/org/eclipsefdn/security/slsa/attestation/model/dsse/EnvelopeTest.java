/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.dsse;

import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnvelopeTest {

    @Test
    public void emptySignatures() {
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("envelope/empty-signatures.jsonl")) {
            List<Envelope> envelopeList = Json.readObjects(inputStream, Envelope.class);

            assertEquals(1, envelopeList.size());
            Envelope envelope = envelopeList.get(0);
            assertEquals(0, envelope.getSignatures().size());
        } catch (IOException ioe) {
            fail();
        }
    }

    @Test
    public void fullSignature() {
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("envelope/full-signature.jsonl")) {
            List<Envelope> envelopeList = Json.readObjects(inputStream, Envelope.class);

            assertEquals(1, envelopeList.size());
            Envelope envelope = envelopeList.get(0);
            assertEquals(1, envelope.getSignatures().size());

            Signature signature = envelope.getSignatures().iterator().next();
            assertEquals("ABCDEFG", signature.getSignature());
            assertEquals("1234567890", signature.getKeyId());
            assertEquals("CERT", signature.getCertificate());
        } catch (IOException ioe) {
            fail();
        }
    }
}
