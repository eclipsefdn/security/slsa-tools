/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SlsaModuleTest {
    @Test
    public void serviceLoader() {
        ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

        try {
            Subject subject = mapper.readValue("{\"name\": \"MyName\", \"digest\": null}", Subject.class);
            assertEquals("MyName", subject.getName());
            assertNotNull(subject.getDigests());
            assertTrue(subject.getDigests().isEmpty());
        } catch (JsonProcessingException ex) {
            fail();
        }
    }

    @Test
    public void withoutModule() {
        ObjectMapper mapper = new ObjectMapper(); //.registerModule(new ParameterNamesModule());

        try {
            Subject subject = mapper.readValue("{\"name\": \"MyName\", \"digest\": null}", Subject.class);
            assertEquals("MyName", subject.getName());
            assertNull(subject.getDigests());
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
            fail();
        }
    }
}
