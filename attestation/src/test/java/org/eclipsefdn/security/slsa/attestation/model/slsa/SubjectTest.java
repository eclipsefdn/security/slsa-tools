/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa;

import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SubjectTest {

    @Test
    public void builderWithoutDigest() {
        Subject subject = Subject.builder().withName("someone").build();

        assertEquals("someone", subject.getName());
        assertNotNull(subject.getDigests());
        Assertions.assertTrue(subject.getDigests().isEmpty());
    }

    @Test
    public void builderWithDigest() {
        Subject subject =
            Subject.builder()
                   .withName("someone")
                   .withDigest(DigestAlgorithm.SHA256, "abcdefg")
                   .build();

        assertEquals("someone", subject.getName());
        assertNotNull(subject.getDigests());
        Assertions.assertEquals(1, subject.getDigests().size());
        Assertions.assertEquals("abcdefg", subject.getDigests().getDigest(DigestAlgorithm.SHA256));
    }
}
