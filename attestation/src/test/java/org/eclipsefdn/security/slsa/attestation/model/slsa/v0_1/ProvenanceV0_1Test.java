/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_1;

import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ProvenanceV0_1Test {

    @ParameterizedTest
    @MethodSource("provenanceFiles")
    public void deserializeProvenanceFile(String resource) {
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resource)) {
            List<ProvenanceStatement> statementList = Json.readObjects(inputStream, ProvenanceStatement.class);
            assertFalse(statementList.isEmpty());
        } catch (IOException e) {
            fail(e);
        }
    }

    private static Stream<Arguments> provenanceFiles() {
        return Stream.of(
            Arguments.of("provenance/v0.1/sample.json")
        );
    }
}
