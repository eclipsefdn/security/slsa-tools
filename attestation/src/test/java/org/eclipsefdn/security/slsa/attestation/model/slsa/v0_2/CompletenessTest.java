/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import org.eclipsefdn.security.slsa.attestation.model.ModelTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CompletenessTest extends ModelTest<Completeness> {

    @Override
    public Completeness getModelFixture() {
        Completeness completeness =
            Completeness.builder()
                        .withCompleteParameters()
                        .withCompleteEnvironment()
                        .withCompleteMaterials()
                        .build();

        return completeness;
    }

    @Test
    public void buildDefaultCompleteness() {
        Completeness completeness =
            Completeness.builder().build();

        assertFalse(completeness.isParametersComplete());
        assertFalse(completeness.isEnvironmentComplete());
        assertFalse(completeness.isMaterialsComplete());
    }

    @Test
    public void buildFullyCompleteness() {
        Completeness completeness = getModelFixture();

        assertTrue(completeness.isParametersComplete());
        assertTrue(completeness.isEnvironmentComplete());
        assertTrue(completeness.isMaterialsComplete());
    }
}
