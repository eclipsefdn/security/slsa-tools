/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import org.eclipsefdn.security.slsa.attestation.model.ModelTest;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConfigSourceTest extends ModelTest<ConfigSource> {

    @Override
    public ConfigSource getModelFixture() {
        ConfigSource configSource =
            ConfigSource.builder()
                        .withUri("https://some.uri")
                        .withDigest(DigestAlgorithm.MD5, "abcdefg")
                        .withEntryPoint("some/entry/point.yml")
                        .build();

        return configSource;
    }

    @Test
    public void buildDefault() {
        ConfigSource configSource =
            ConfigSource.builder().build();

        assertNull(configSource.getUri());
        assertTrue(configSource.getDigests().isEmpty());
        assertNull(configSource.getEntryPoint());
    }

    @Test
    public void buildSpecific() {
        ConfigSource configSource = getModelFixture();

        assertEquals("https://some.uri", configSource.getUri());
        assertEquals("abcdefg", configSource.getDigests().getDigest(DigestAlgorithm.MD5));
        assertEquals("some/entry/point.yml", configSource.getEntryPoint());
    }
}
