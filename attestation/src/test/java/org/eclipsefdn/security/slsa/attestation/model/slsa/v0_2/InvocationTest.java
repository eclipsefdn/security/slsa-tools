/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import org.eclipsefdn.security.slsa.attestation.model.ModelTest;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestAlgorithm;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class InvocationTest extends ModelTest<Invocation> {

    private ConfigSource getConfigSource() {
        ConfigSource configSource =
            ConfigSource.builder()
                        .withUri("https://some.uri")
                        .withDigest(DigestAlgorithm.MD5, "abcdefg")
                        .withEntryPoint("some/entry/point.yml")
                        .build();
        return configSource;
    }

    @Override
    public Invocation getModelFixture() {
        Invocation invocation =
            Invocation.builder()
                      .withConfigSource(getConfigSource())
                      .withParameters(Collections.singletonMap("key1", "value1"))
                      .withEnvironment(Collections.singletonMap("key2", "value2"))
                      .build();

        return invocation;
    }

    @Test
    public void buildDefault() {
        Invocation invocation =
            Invocation.builder().build();

        assertNull(invocation.getConfigSource());
        assertTrue(invocation.getParameters().isEmpty());
        assertTrue(invocation.getEnvironment().isEmpty());
    }

    @Test
    public void buildSpecific() {
        Invocation invocation =
            Invocation.builder()
                      .withConfigSource(getConfigSource())
                      .withParameters(Collections.singletonMap("key1", "value1"))
                      .withEnvironment(Collections.singletonMap("key2", "value2"))
                      .build();

        assertEquals(getConfigSource(), invocation.getConfigSource());
        assertEquals(Collections.singletonMap("key1", "value1"), invocation.getParameters());
        assertEquals(Collections.singletonMap("key2", "value2"), invocation.getEnvironment());
    }
}
