/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.eclipsefdn.security.slsa.attestation.model.ModelTest;
import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class MetadataTest extends ModelTest<Metadata> {

    private Completeness getCompleteness() {
        Completeness completeness =
            Completeness.builder()
                        .withCompleteParameters()
                        .withCompleteEnvironment()
                        .withCompleteMaterials()
                        .build();
        return completeness;
    }

    @Override
    public Metadata getModelFixture() {
        Metadata metadata =
            Metadata.builder()
                    .withBuildInvocationId("1234")
                    .withBuildStartedOn(OffsetDateTime.parse("2023-01-01T10:10:10Z"))
                    .withBuildFinishedOn(OffsetDateTime.parse("2023-01-01T10:10:20Z"))
                    .withCompleteness(getCompleteness())
                    .withReproducible(true)
                    .build();

        return metadata;
    }

    @Test
    public void buildFixture() {
        Metadata metadata = getModelFixture();

        assertEquals("1234", metadata.getBuildInvocationId());
        assertEquals(OffsetDateTime.parse("2023-01-01T10:10:10Z"), metadata.getBuildStartedOn());
        assertEquals(OffsetDateTime.parse("2023-01-01T10:10:20Z"), metadata.getBuildFinishedOn());
        assertEquals(getCompleteness(), metadata.getCompleteness());
        assertTrue(metadata.isReproducible());
    }

    @Test
    public void caseInsensitiveProperties() {
        try {
            Metadata metadata = Json.readObject("{\"buildInvocationID\":\"1234\"}", Metadata.class);
            assertEquals("1234", metadata.getBuildInvocationId());
        } catch (JsonProcessingException ex) {
            fail();
        }
    }
}
