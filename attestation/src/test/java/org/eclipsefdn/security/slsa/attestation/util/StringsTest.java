/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.attestation.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StringsTest {

    @Test
    public void testRemovePrefix() {
        assertThrows(NullPointerException.class, () -> {
            Strings.removePrefix(null, null);
        });

        assertEquals("abc", Strings.removePrefix("abc", "-"));
        assertEquals("abc", Strings.removePrefix("-abc", "-"));
    }

    @Test
    public void testRemoveSuffix() {
        assertThrows(NullPointerException.class, () -> {
            Strings.removeSuffix(null, null);
        });

        assertEquals("abc", Strings.removeSuffix("abc", "-"));
        assertEquals("abc", Strings.removeSuffix("abc-", "-"));
    }

    @Test
    public void testIsValidSemVer() {
        assertTrue(Strings.isValidSemVer("v1.0.0"));

        assertFalse(Strings.isValidSemVer("1.0.0"));
        assertFalse(Strings.isValidSemVer("1.0"));
        assertFalse(Strings.isValidSemVer("v1.0"));

        assertFalse(Strings.isValidSemVer("va.b.c"));
        assertFalse(Strings.isValidSemVer("v1.0.-1"));

        assertTrue(Strings.isValidSemVer("v2.3.4"));
    }
}
