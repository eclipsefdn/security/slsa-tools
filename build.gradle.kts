plugins {
    id("base")
    id("io.github.gradle-nexus.publish-plugin") version "1.3.0"
}

allprojects {
    group = "org.eclipsefdn.security.slsa"
    version = "0.2-SNAPSHOT"

    extra["isReleaseVersion"] = !version.toString().endsWith("SNAPSHOT")

    repositories {
        mavenCentral()
    }
}

nexusPublishing {
    repositories {
        create("EclipseFdnNexus") {
            // Hack to support Nexus OSS due to https://github.com/gradle-nexus/publish-plugin/issues/149
            if (project.extra["isReleaseVersion"] as Boolean) {
                nexusUrl.set(uri("https://repo.eclipse.org/content/repositories/slsa-tools-releases/"))
                snapshotRepositoryUrl.set(uri("https://repo.eclipse.org/content/repositories/slsa-tools-releases/"))
                useStaging.set(false)
            } else {
                nexusUrl.set(uri("https://repo.eclipse.org/content/repositories/slsa-tools-releases/"))
                snapshotRepositoryUrl.set(uri("https://repo.eclipse.org/content/repositories/slsa-tools-snapshots/"))
            }
        }
    }
}
