import org.gradle.api.publish.maven.MavenPublication

plugins {
    id("java-library")
    id("module")
    id("maven-publish")
    id("signing")
}

publishing {
    publications {
        create<MavenPublication>(project.name) {
            from(components["java"])
            groupId    = project.group.toString()
            artifactId = project.name
            version    = project.version.toString()

            pom {
                name.set(project.name)
                description.set(project.description)
                url.set("https://gitlab.eclipse.org/netomi/slsa-tools")
                licenses {
                    license {
                        name.set("Eclipse Public License, Version 2.0")
                        url.set("https://www.eclipse.org/legal/epl-2.0/")
                    }
                }
                developers {
                    developer {
                        id.set("netomi")
                        name.set("Thomas Neidhart")
                        email.set("thomas.neidhart@eclipse-foundation.org")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.eclipse.org/netomi/slsa-tools.git")
                    developerConnection.set("scm:git:git@gitlab.eclipse.org:netomi/slsa-tools.git")
                    url.set("https://gitlab.eclipse.org/netomi/slsa-tools")
                }
            }
        }
    }

    repositories {
        mavenLocal()
    }
}

signing {
    setRequired({
        (project.extra["isReleaseVersion"] as Boolean) && gradle.taskGraph.hasTask("publish")
    })

    useGpgCmd()
    sign(publishing.publications[project.name])
}

tasks.withType<Sign>().configureEach {
    onlyIf { project.extra["isReleaseVersion"] as Boolean }
}
