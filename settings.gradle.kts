rootProject.name = "slsa-tools"

include("attestation")
include("tools")
include("verification")

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")