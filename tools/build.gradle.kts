plugins {
    id("module")
    id("distribution")
}

base {
    archivesName.set("slsa-tools")
}

distributions {
    main {
        distributionBaseName.set("slsa-tools")
        contents {
            into("lib") {
                from(tasks["jar"])
                from(configurations.runtimeClasspath)
            }
            into("") {
                from("${projectDir}/scripts")
                eachFile {
                    fileMode = 0b111101101
                }
            }
        }
    }
}

dependencies {
    implementation(projects.attestation)
    implementation(projects.verification)

    implementation(libs.picocli)
    implementation(libs.syntaxhighlighter)

    implementation(libs.jackson.databind)
}
