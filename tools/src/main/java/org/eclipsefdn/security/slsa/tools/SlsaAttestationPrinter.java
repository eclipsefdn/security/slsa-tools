/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.tools;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.skjolber.jackson.jsh.*;
import org.eclipsefdn.security.slsa.attestation.error.InvalidModelException;
import org.eclipsefdn.security.slsa.attestation.io.AttestationReader;
import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.eclipsefdn.security.slsa.attestation.model.SignedAttestation;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import picocli.CommandLine;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static picocli.CommandLine.*;

@Command(name                 = "slsa-dump",
         description          = "Prints the provenance content of slsa attestation files.",
         parameterListHeading = "%nParameters:%n",
         optionListHeading    = "%nOptions:%n")
public class SlsaAttestationPrinter implements Runnable {

    @Parameters(index = "0", arity = "1", paramLabel = "inputfile", description = "inputfile to process (*.jsonl)")
    private Path inputFile;

    @Option(names = { "--raw" }, description = "prints the provenance content as unprocessed json")
    private boolean raw;

    @Option(names = { "-M", "--monochrome" }, description = "disables color output")
    private boolean monochrome;

    @Override
    public void run() {
        if (!Files.exists(inputFile)) {
            System.err.printf("Input file '%s' does not exist.%n", inputFile);
            System.exit(1);
        }

        try(InputStream inputStream = Files.newInputStream(inputFile)) {
            AttestationReader reader = new AttestationReader();

            List<SignedAttestation> attestationList = reader.readAttestations(inputStream);
            for (SignedAttestation attestation : attestationList) {
                JsonGenerator jsonGenerator;
                Object content;

                if (raw) {
                    // in raw mode, we just decode they payload and dump the json with a pretty printer.
                    byte[] decodedPayload = attestation.getEnvelope().getDecodedPayload();
                    content = Json.MAPPER.readValue(decodedPayload, Map.class);
                    jsonGenerator = Json.MAPPER.createGenerator(System.out);
                } else {
                    // in normal mode, we read the payload json into our model class and dump the result.
                    // unrecognized keys are omitted and will not be visible in the output.
                    content = attestation.getStatement();
                    ObjectWriter writer = Json.MAPPER.writerFor(ProvenanceStatement.class);
                    jsonGenerator = writer.createGenerator(System.out);
                }

                // wrap the json generator with a syntax highlighter.
                if (!monochrome) {
                    SyntaxHighlighter highlighter =
                            DefaultSyntaxHighlighter
                                .newBuilder()
                                .withField(AnsiSyntaxHighlight.BLUE)
                                .withString(AnsiSyntaxHighlight.GREEN)
                                .withCurlyBrackets(AnsiSyntaxHighlight.HIGH_INTENSITY)
                                .build();

                    jsonGenerator = new SyntaxHighlightingJsonGenerator(jsonGenerator, highlighter, true);
                } else {
                    jsonGenerator = jsonGenerator.useDefaultPrettyPrinter();
                }

                jsonGenerator.writeObject(content);

                System.out.println();
            }
        } catch (IOException | InvalidModelException ex) {
            System.err.printf("Failed to read file '%s': %s%n", inputFile, ex.getMessage());
        }
    }

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new SlsaAttestationPrinter());
        commandLine.execute(args);
    }
}
