/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.tools;

import org.eclipsefdn.security.slsa.attestation.model.Artifact;
import org.eclipsefdn.security.slsa.verification.Verifier;
import picocli.CommandLine;

import java.nio.file.Files;
import java.nio.file.Path;

@CommandLine.Command(name                 = "slsa-verify",
                     description          = "Verifies a given SLSA attestation for an artifact.",
                     parameterListHeading = "%nParameters:%n",
                     optionListHeading    = "%nOptions:%n",
                     usageHelpWidth       = 130)
public class SlsaVerifier implements Runnable {

    @CommandLine.Parameters(index = "0", arity = "1..*", description = "artifact(s) to verify")
    private Path[] artifacts;

    @CommandLine.Option(names = { "--provenance-path" }, required = true, paramLabel = "PATH",
                        description = "path to a provenance file, e.g. path/to/artifact.intoto.jsonl")
    private Path provenancePath;

    @CommandLine.Option(names = { "--source-uri" }, required = true, paramLabel = "SOURCE-URI",
                        description = "expected source repository that should have produced the binary, e.g. github.com/some/repo")
    private String sourceUri;

    @CommandLine.Option(names = { "--source-branch" }, paramLabel = "SOURCE-BRANCH",
                        description = "[optional] expected branch the binary was built from")
    private String sourceBranch;

    @CommandLine.Option(names = { "--source-tag" }, paramLabel = "SOURCE-TAG",
                        description = "[optional] expected tag the binary was built from")
    private String sourceTag;

//    Flags:
//            --build-workflow-input map[]    [optional] a workflow input provided by a user at trigger time in the format 'key=value'. (Only for 'workflow_dispatch' events). (default map[])
//            --builder-id string             [optional] the unique builder ID who created the provenance
//      --source-versioned-tag string   [optional] expected version the binary was compiled from. Uses semantic version to match the tag

    @Override
    public void run() {
        if (!Files.exists(provenancePath)) {
            printError("Provenance file does not exist: " + provenancePath);
            return;
        }

        //List<SignedAttestation> attestations = new AttestationReader().readAttestations()
        Verifier verifier = new Verifier();

        for (Path artifactPath : artifacts) {
            printMessage(CommandLine.Help.Ansi.AUTO.string("Verifying artifact @|italic,white " + artifactPath + "|@ "));
            Artifact artifact = Artifact.of(artifactPath);

            //slsaVerifier.verify(artifact, sourceUri, );
        }

//        ProvenanceStatement stmt = Json.readObject(Files.readAllBytes(Paths.get("tmp/github.json")), ProvenanceStatement.class);

        //List<SignedAttestation> attestations = new AttestationReader().readAttestations(new FileInputStream(input));

//        for (SignedAttestation attestation : attestations) {
//            new Verifier().verify(Artifact.of(Paths.get(artifact)), sourceURI, attestation);

    }

    private static void printError(String message) {
        System.err.println(CommandLine.Help.Ansi.AUTO.string("@|bold,red [ERR]|@ ") + message);
    }

    private static void printMessage(String message) {
        System.out.println(message);
    }

    public static void main(String[] args) {
        CommandLine commandLine = new CommandLine(new SlsaVerifier());
        commandLine.execute(args);
    }
}