plugins {
    id("library")
}

description = "Library to verify SLSA provenance attestation files"

dependencies {
    implementation(projects.attestation)

    implementation(libs.bouncycastle)
    //implementation(libs.sigstore)
}
