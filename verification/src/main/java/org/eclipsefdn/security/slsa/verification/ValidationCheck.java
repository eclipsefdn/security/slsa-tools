/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification;

public class ValidationCheck {
    private String target;
    private String message;
    private ValidationStatus status;

    public static ValidationCheck ofSuccess(String target, String message) {
        return new ValidationCheck(target, message, ValidationStatus.PASSED);
    }

    public static ValidationCheck ofFailure(String target, String message) {
        return new ValidationCheck(target, message, ValidationStatus.FAILED);
    }

    private ValidationCheck(String target, String message, ValidationStatus status) {
        this.target  = target;
        this.message = message;
        this.status  = status;
    }

    public String getTarget() {
        return target;
    }

    public String getMessage() {
        return message;
    }

    public ValidationStatus getStatus() {
        return status;
    }

    public String getValidationMessage() {
        return String.format("%s at '%s'", message, target);
    }

    @Override
    public String toString() {
        return String.format("ValidationCheck[target=%s,message=%s,status=%s]", target, message, status);
    }
}
