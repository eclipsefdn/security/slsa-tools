/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification;

import org.eclipsefdn.security.slsa.attestation.model.Artifact;
import org.eclipsefdn.security.slsa.attestation.model.dsse.Envelope;

import java.util.*;
import java.util.stream.Collectors;

public class ValidationContext {
    private final Artifact artifact;
    private final Envelope envelope;

    private final String sourceURI;

    private final Map<String, List<ValidationCheck>> checksByCategory = new LinkedHashMap<>();

    public static Builder builder() {
        return new Builder();
    }

    private ValidationContext(Artifact artifact, Envelope envelope, String sourceURI) {
        this.artifact = artifact;
        this.envelope = envelope;
        this.sourceURI = sourceURI;
    }

    public Artifact getArtifact() {
        return artifact;
    }

    public Envelope getEnvelope() {
        return envelope;
    }

    public String getSourceURI() {
        return sourceURI;
    }

    public void addCheck(String category, ValidationCheck check) {
        getOrAddChecks(category).add(check);
    }

    public Collection<String> getCategories() {
        return checksByCategory.keySet();
    }

    public Collection<ValidationCheck> getFailures() {
        return
            checksByCategory.entrySet()
                            .stream()
                            .flatMap(entry -> entry.getValue().stream())
                            .filter(c -> c.getStatus() == ValidationStatus.FAILED)
                            .collect(Collectors.toList());
    }

    public Collection<ValidationCheck> getChecks(String category) {
        return checksByCategory.getOrDefault(category, Collections.emptyList());
    }

    private List<ValidationCheck> getOrAddChecks(String category) {
        return checksByCategory.computeIfAbsent(category, (key) -> new ArrayList<>() );
    }

    @Override
    public String toString() {
        return String.format("ValidationContext[%s]", checksByCategory);
    }

    public static class Builder {
        private Artifact artifact;
        private Envelope envelope;
        private String sourceURI;

        public Builder withArtifact(Artifact artifact) {
            this.artifact = artifact;
            return this;
        }

        public Builder withEnvelope(Envelope envelope) {
            this.envelope = envelope;
            return this;
        }

        public Builder withSourceURI(String sourceURI) {
            this.sourceURI = sourceURI;
            return this;
        }

        public ValidationContext build() {
            return new ValidationContext(artifact, envelope, sourceURI);
        }
    }
}
