/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification;

import org.eclipsefdn.security.slsa.verification.validator.ProvenanceV0_2Validator;
import org.eclipsefdn.security.slsa.verification.validator.StatementValidator;
import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderValidator;
import org.eclipsefdn.security.slsa.verification.validator.builder.gha.GHABuilderValidator;
import org.eclipsefdn.security.slsa.attestation.model.slsa.Provenance;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2.ProvenanceV0_2;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public class Validators {
    private static final Map<String, Validator<? extends Provenance>> provenanceValidators = new HashMap<>();

    private static final List<BuilderValidator> builderValidators = new ArrayList<>();

    static {
        provenanceValidators.put(ProvenanceV0_2.TYPE, new ProvenanceV0_2Validator());

        builderValidators.add(new GHABuilderValidator());
    }

    public static Validator<ProvenanceStatement> getStatementValidator() {
        return new StatementValidator();
    }

    public static <T extends Provenance> Validator<T> getProvenanceValidator(String provenanceType) {
        return (Validator<T>) provenanceValidators.get(provenanceType);
    }

    public static BuilderValidator getBuilderValidator(String builderId) {
        for (BuilderValidator validator : builderValidators) {
            if (validator.isAuthorativeFor(builderId)) {
                return validator;
            }
        }
        return null;
    }

    private Validators() {}

    public static boolean requireNonNull(String category, String target, Object obj, ValidationContext context) {
        if (obj == null) {
            context.addCheck(category, ValidationCheck.ofFailure(target, "required object is null"));
            return false;
        } else {
            return true;
        }
    }

    public static boolean requireNonEmpty(String category, String target, Collection<?> collection, ValidationContext context) {
        if (collection.isEmpty()) {
            context.addCheck(category, ValidationCheck.ofFailure(target, "collection is empty"));
            return false;
        } else {
            return true;
        }
    }

    public static boolean requireValidURI(String category, String target, String uri, ValidationContext context) {
        if (!isValidURI(uri)) {
            context.addCheck(category, ValidationCheck.ofFailure(target, String.format("invalid uri '%s'", uri)));
            return false;
        } else {
            return true;
        }
    }

    private static boolean isValidURI(String uri) {
        try {
            URL url = new URI(uri).toURL();
            return true;
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
    }
}
