/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification;

import org.eclipsefdn.security.slsa.attestation.model.Artifact;
import org.eclipsefdn.security.slsa.attestation.model.SignedAttestation;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;

public class Verifier {

    public Verifier() {}

    public void verify(Artifact artifact, String sourceURI, SignedAttestation attestation) {
        ValidationContext context =
            ValidationContext.builder()
                             .withArtifact(artifact)
                             .withEnvelope(attestation.getEnvelope())
                             .withSourceURI(sourceURI)
                             .build();

        ProvenanceStatement statement = attestation.getStatement();
        Validators.getStatementValidator().validate(statement, context);

        for (String category : context.getCategories()) {
            System.out.printf("%s%n", category);
            System.out.println("------------------------------");
            for (ValidationCheck check : context.getChecks(category)) {
                System.out.println(check);
            }
        }
    }
}
