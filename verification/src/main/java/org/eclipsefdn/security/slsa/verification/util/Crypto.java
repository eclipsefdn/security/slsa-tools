/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.util;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Crypto {
    private Crypto() {}

    public static List<String> getURIs(X509Certificate x509Certificate) {
        try {
            Collection<List<?>> altNames = x509Certificate.getSubjectAlternativeNames();
            if (altNames == null) {
                return Collections.emptyList();
            }

            List<String> uriList = new ArrayList<>();
            for (List<?> item : altNames) {
                Integer type = (Integer) item.get(0);
                if (type == 6) {
                    Object data = item.get(1);
                    if (data instanceof String) {
                        uriList.add((String) data);
                    }
                }
            }

            return uriList;
        } catch (CertificateException ex) {
            return Collections.emptyList();
        }
    }

    public static String getStringExtensionValue(X509Certificate x509Certificate, String oid) throws IllegalArgumentException {
        byte[] extensionValue = x509Certificate.getExtensionValue(oid);
        if (extensionValue != null) {
            try {
                ASN1Primitive derObject = unmarshalASN1(extensionValue);
                if (derObject instanceof DEROctetString) {
                    return new String(((DEROctetString) derObject).getOctets());
                } else {
                    throw new IllegalArgumentException(String.format("extension value for oid '%s' not an octet string", oid));
                }
            } catch (IOException ex) {
                throw new IllegalArgumentException(String.format("unable to retrieve extension value for oid '%s'", oid), ex);
            }
        }

        return null;
    }

    private static ASN1Primitive unmarshalASN1(byte[] data) throws IOException {
        try (ASN1InputStream is = new ASN1InputStream(new ByteArrayInputStream(data))) {
            return is.readObject();
        }
    }
}
