/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator;

import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderValidator;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2.ProvenanceV0_2;
import org.eclipsefdn.security.slsa.verification.ValidationCheck;
import org.eclipsefdn.security.slsa.verification.ValidationContext;
import org.eclipsefdn.security.slsa.verification.Validator;
import org.eclipsefdn.security.slsa.verification.Validators;

public class ProvenanceV0_2Validator implements Validator<ProvenanceV0_2> {
    @Override
    public void validate(ProvenanceV0_2 provenance, ValidationContext context) {
        Validators.requireNonNull("schema", "predicate.buildType", provenance.getBuildType(), context);

        if (Validators.requireNonNull("schema", "predicate.builder", provenance.getBuilder(), context)) {
            String builderId = provenance.getBuilder().getId();

            if (Validators.requireNonNull("schema", "predicate.builder.id", builderId, context)) {
                Validators.requireValidURI("schema", "predicate.builder.id", builderId, context);

                BuilderValidator builderValidator = Validators.getBuilderValidator(builderId);
                if (builderValidator == null) {
                    String message = String.format("untrusted builder with id '%s'", builderId);
                    context.addCheck("builder", ValidationCheck.ofFailure("predicate.builder", message));
                } else {
                    String message = String.format("validate using trusted builder with name '%s'", builderValidator.getName());
                    context.addCheck("builder", ValidationCheck.ofSuccess("predicate.builder", message));

                    builderValidator.validate(null, context);
                }
            }
        }
    }
}
