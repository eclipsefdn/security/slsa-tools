/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator;

import org.eclipsefdn.security.slsa.attestation.util.Digests;
import org.eclipsefdn.security.slsa.attestation.model.slsa.Provenance;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import org.eclipsefdn.security.slsa.attestation.model.slsa.Subject;
import org.eclipsefdn.security.slsa.attestation.model.slsa.common.DigestSet;
import org.eclipsefdn.security.slsa.verification.Validator;
import org.eclipsefdn.security.slsa.verification.ValidationCheck;
import org.eclipsefdn.security.slsa.verification.ValidationContext;
import org.eclipsefdn.security.slsa.verification.Validators;

import java.nio.file.Files;
import java.nio.file.Path;

public class StatementValidator implements Validator<ProvenanceStatement> {
    @Override
    public void validate(ProvenanceStatement provenance, ValidationContext context) {

        // Check if the provenance contains any subject.
        // If not, there is no need to further validate.
        if (!Validators.requireNonEmpty("schema", "subject", provenance.getSubjects(), context)) {
            return;
        }

        // Check if the artifact matches any subject in the provenance.
        // If not, no need to further validate.
        if (!verifyArtifactHash(provenance, context)) {
            return;
        }

        if (Validators.requireNonNull("schema", "predicateType", provenance.getPredicateType(), context)) {
            if (Validators.requireNonNull("schema", "predicate", provenance.getProvenance(), context)) {
                Validator<Provenance> validator = Validators.getProvenanceValidator(provenance.getPredicateType());
                if (validator == null) {
                    String error = String.format("invalid predicateType '%s'", provenance.getPredicateType());
                    context.addCheck("attestation", ValidationCheck.ofFailure("predicate", error));
                } else {
                    validator.validate(provenance.getProvenance(), context);
                }
            }
        }
    }

    private boolean verifyArtifactHash(ProvenanceStatement statement, ValidationContext context) {
        Path artifactPath = context.getArtifact().getPath();

        if (!Files.exists(artifactPath)) {
            String error = String.format("artifact with path '%s' does not exist", artifactPath);
            context.addCheck("envelope", ValidationCheck.ofFailure("hash", error));
            return false;
        } else {
            Subject matchingSubject = null;
            String sha256Checksum = Digests.getSHA256Digest(artifactPath);

            for (Subject subject : statement.getSubjects()) {
                DigestSet digestSet = subject.getDigests();

                if (!digestSet.hasDigest("sha256")) {
                    String error = String.format("no sha256 digest present for subject '%s'", subject.getName());
                    context.addCheck("statement", ValidationCheck.ofFailure("subject", error));
                } else {
                    String subjectChecksum = digestSet.getDigest("sha256");
                    if (sha256Checksum.equals(subjectChecksum)) {
                        matchingSubject = subject;
                        break;
                    }
                }
            }

            if (matchingSubject != null) {
                String message = String.format("artifact hash matched with subject of name '%s'", matchingSubject.getName());
                context.addCheck("attestation", ValidationCheck.ofSuccess("artifact", message));
                return true;
            } else {
                context.addCheck("attestation", ValidationCheck.ofFailure("artifact", "artifact hash does not match any subject"));
                return false;
            }
        }
    }
}
