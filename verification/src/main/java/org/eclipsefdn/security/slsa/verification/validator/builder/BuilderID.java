/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator.builder;

import org.eclipsefdn.security.slsa.attestation.error.Errors;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents a builder ID consisting of a name and a version part.
 */
public class BuilderID {
    private static final Pattern BUILDER_ID_REGEX = Pattern.compile("([^@]+)(@([^@]+))?");

    private final String name;
    private final String version;

    public static BuilderID parseFromString(String builderID) {
        Objects.requireNonNull(builderID);

        Matcher matcher = BUILDER_ID_REGEX.matcher(builderID);
        if (matcher.matches()) {
            String name    = matcher.group(1);
            String version = matcher.group(3);
            return new BuilderID(name, version);
        } else {
            throw new IllegalArgumentException(Errors.INVALID_BUILDER_ID_FORMAT);
        }
    }

    private BuilderID(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public String toString() {
        return String.format("BuilderID[name='%s',version='%s']", name, version);
    }
}
