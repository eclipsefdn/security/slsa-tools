/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator.builder.gha;

import org.eclipsefdn.security.slsa.attestation.error.Errors;
import org.eclipsefdn.security.slsa.attestation.model.dsse.Signature;
import org.eclipsefdn.security.slsa.attestation.model.dsse.Envelope;
import org.eclipsefdn.security.slsa.attestation.util.Strings;
import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderData;
import org.eclipsefdn.security.slsa.verification.ValidationCheck;
import org.eclipsefdn.security.slsa.verification.ValidationContext;
import org.eclipsefdn.security.slsa.verification.Validators;
import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderID;
import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderValidator;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

public class GHABuilderValidator implements BuilderValidator {
    private static final String FQ_BUILDER_REPOSITORY = "https://github.com/slsa-framework/slsa-github-generator";

    public static final String TRUSTED_BUILDER_REPOSITORY = "slsa-framework/slsa-github-generator";
    public static final String CERTIFICATE_OIDC_ISSUER    = "https://token.actions.githubusercontent.com";

    public static final Map<String, Boolean> DEFAULT_ARTIFACT_TRUSTED_REUSABLE_WORKFLOWS;

    static {
        Map<String, Boolean> workflowMap = new HashMap<>();

        workflowMap.put(FQ_BUILDER_REPOSITORY + "/.github/workflows/generator_generic_slsa3.yml", true);
        workflowMap.put(FQ_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml", true);

        DEFAULT_ARTIFACT_TRUSTED_REUSABLE_WORKFLOWS = workflowMap;
    }

    @Override
    public String getName() {
        return FQ_BUILDER_REPOSITORY;
    }

    @Override
    public boolean isAuthorativeFor(String builderId) {
        return builderId != null && builderId.startsWith(FQ_BUILDER_REPOSITORY);
    }

    @Override
    public void validate(BuilderData object, ValidationContext context) {
        X509Certificate certificate = getCertificateFromEnvelope(context.getEnvelope(), context);
        if (certificate != null) {
            validateWorkflowIdentity(WorkflowIdentity.fromCertificate(certificate), context);
        }
    }

    public void validateWorkflowIdentity(WorkflowIdentity workflowIdentity, ValidationContext context) {
        String jobWorkflowRef = workflowIdentity.getJobWorkflowRef();
        if (Validators.requireNonNull("builder", "jobWorkflowRef", jobWorkflowRef, context)) {
            try {
                String fqBuilderName =
                    jobWorkflowRef.startsWith("https://github.com/") ?
                        jobWorkflowRef :
                        "https://github.com/" + jobWorkflowRef;

                BuilderID certificateBuilderId = BuilderID.parseFromString(fqBuilderName);

                // TODO: support passing in a trusted builder via the ValidationContext.
                if (!DEFAULT_ARTIFACT_TRUSTED_REUSABLE_WORKFLOWS.getOrDefault(certificateBuilderId.getName(), false)) {
                    context.addCheck("builder", ValidationCheck.ofFailure("jobWorkflowRef", Errors.UNTRUSTED_REUSABLE_WORKFLOW));
                } else {
                    String version = certificateBuilderId.getVersion();
                    version = version != null ? Strings.removePrefix(version, "refs/tags/") : null;

                    if (Strings.isValidSemVer(version)) {
                        context.addCheck("builder",
                                         ValidationCheck.ofSuccess("jobWorkflowRef",
                                                                   String.format("trusted builder '%s'", fqBuilderName)));
                    } else {
                        context.addCheck("builder",
                                         ValidationCheck.ofFailure("jobWorkflowRef",
                                                                   Errors.UNTRUSTED_REUSABLE_WORKFLOW_INVALID_VERSION));
                    }
                }
            } catch (IllegalArgumentException ex) {
                context.addCheck("builder", ValidationCheck.ofFailure("jobWorkflowRef", Errors.MALFORMED_URI));
            }
        }

        String issuer = workflowIdentity.getIssuer();
        if (!CERTIFICATE_OIDC_ISSUER.equals(issuer)) {
            context.addCheck("builder", ValidationCheck.ofFailure("certificate.issuer", Errors.INVALID_OIDC_ISSUER));
        }

        String callerRepository = workflowIdentity.getCallerRepository();
        if (Validators.requireNonNull("builder", "callerRepository", callerRepository, context)) {
            // The caller repository in the x509 certificate extension is not fully qualified.
            // It is only comprised of '{organization}/{repository}'.
            String expectedSource = context.getSourceURI();

            expectedSource = Strings.removePrefix(expectedSource, "git+https://");
            expectedSource = Strings.removePrefix(expectedSource, "github.com/");

            if (!expectedSource.equals(callerRepository)) {
                context.addCheck("builder", ValidationCheck.ofFailure("callerRepository", Errors.MISMATCH_SOURCE_URI));
            }
        }
    }

    private X509Certificate getCertificateFromEnvelope(Envelope envelope, ValidationContext context) {
        int index = 0;
        for (Signature signature : envelope.getSignatures()) {
            try {
                return signature.getX509Certificate();
            } catch (CertificateException ex) {
                context.addCheck("envelope",
                                 ValidationCheck.ofFailure(String.format("signature[%d].cert", index),
                                                           "invalid certificate"
                ));
            }
            index++;
        }
        return null;
    }
}
