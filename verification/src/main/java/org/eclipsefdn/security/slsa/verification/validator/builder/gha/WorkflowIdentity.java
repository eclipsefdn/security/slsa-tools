/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator.builder.gha;

import org.eclipsefdn.security.slsa.verification.util.Crypto;

import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Objects;

public class WorkflowIdentity {
    private final String callerRepository;
    private final String callerHash;
    private final String jobWorkflowRef;
    private final String trigger;
    private final String issuer;

    public static WorkflowIdentity fromCertificate(X509Certificate certificate) {
        Objects.requireNonNull(certificate);

        String callerRepository = Crypto.getStringExtensionValue(certificate, "1.3.6.1.4.1.57264.1.5");
        String callerHash       = Crypto.getStringExtensionValue(certificate, "1.3.6.1.4.1.57264.1.3");
        String issuer           = Crypto.getStringExtensionValue(certificate, "1.3.6.1.4.1.57264.1.1");
        String trigger          = Crypto.getStringExtensionValue(certificate, "1.3.6.1.4.1.57264.1.2");

        List<String> uris = Crypto.getURIs(certificate);
        String jobWorkflowRef   = uris.size() == 1 ? uris.get(0) : null;

        return new WorkflowIdentity(callerRepository, callerHash, jobWorkflowRef, trigger, issuer);
    }

    public static WorkflowIdentity of(String callerRepository, String callerHash, String jobWorkflowRef, String trigger, String issuer) {
        return new WorkflowIdentity(callerRepository, callerHash, jobWorkflowRef, trigger, issuer);
    }

    private WorkflowIdentity(String callerRepository, String callerHash, String jobWorkflowRef, String trigger, String issuer) {
        this.callerRepository = callerRepository;
        this.callerHash = callerHash;
        this.jobWorkflowRef = jobWorkflowRef;
        this.trigger = trigger;
        this.issuer = issuer;
    }

    public String getCallerRepository() {
        return callerRepository;
    }

    public String getCallerHash() {
        return callerHash;
    }

    public String getJobWorkflowRef() {
        return jobWorkflowRef;
    }

    public String getTrigger() {
        return trigger;
    }

    public String getIssuer() {
        return issuer;
    }

    @Override
    public String toString() {
        return String.format("WorkflowIdentity[callerRepository=%s,callerHash=%s,jobWorkflowRef=%s,trigger=%s,issuer=%s]",
                             callerRepository,
                             callerHash,
                             jobWorkflowRef,
                             trigger,
                             issuer);
    }
}
