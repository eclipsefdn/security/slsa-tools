/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator;

import org.eclipsefdn.security.slsa.attestation.model.Artifact;
import org.eclipsefdn.security.slsa.attestation.model.slsa.v0_2.ProvenanceV0_2;
import org.eclipsefdn.security.slsa.attestation.util.Json;
import org.eclipsefdn.security.slsa.attestation.model.slsa.ProvenanceStatement;
import org.eclipsefdn.security.slsa.verification.ValidationContext;
import org.eclipsefdn.security.slsa.verification.ValidationCheck;
import org.eclipsefdn.security.slsa.verification.Validators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ProvenanceV0_2ValidatorTest {

    @ParameterizedTest
    @MethodSource("provenanceFiles")
    public void verifyProvenance(String resource, List<String> expectedFailures) {
        try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(resource)) {
            List<ProvenanceStatement> statementList = Json.readObjects(inputStream, ProvenanceStatement.class);
            assertEquals(1, statementList.size());

            ProvenanceStatement statement = statementList.get(0);
            Assertions.assertEquals(ProvenanceV0_2.TYPE, statement.getPredicateType());

            ValidationContext context =
                ValidationContext.builder()
                                 .withArtifact(Artifact.of(Paths.get("non-existent-file")))
                                 .build();

            Validators.getProvenanceValidator(ProvenanceV0_2.TYPE).validate(statement.getProvenance(), context);

            int expectedFailureCount = expectedFailures == null ? 0 : expectedFailures.size();
            assertEquals(expectedFailureCount, context.getFailures().size());

            if (expectedFailureCount > 0) {
                Set<String> actualErrors =
                    context.getFailures()
                           .stream()
                           .map(ValidationCheck::getMessage)
                           .collect(Collectors.toSet());

                for (String error : expectedFailures) {
                    assertTrue(actualErrors.contains(error), error);
                }
            }
        } catch (IOException e) {
            fail(e);
        }
    }

    private static Stream<Arguments> provenanceFiles() {
        return Stream.of(
            Arguments.of("provenance/v0.2/untrusted-builder.json",
                         Arrays.asList("untrusted builder with id 'mailto:person@example.com'")),
            Arguments.of("provenance/v0.2/missing-buildtype.json",
                         Arrays.asList("required object is null",
                                       "untrusted builder with id 'https://github.com/Attestations/GitHubHostedActions@v1'")),
            Arguments.of("provenance/v0.2/missing-builder.json",
                         Arrays.asList("required object is null"))
        );
    }
}
