/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator.builder;

import org.eclipsefdn.security.slsa.verification.validator.builder.BuilderID;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.*;

public class BuilderIDTest {

    @ParameterizedTest
    @MethodSource("parseSpecs")
    public void parse(String builderIDString,
                      String expectedName,
                      String expectedVersion,
                      Class<? extends Throwable> expectedException)
    {
            if (expectedException != null) {
                assertThrows(expectedException, () -> {
                    BuilderID.parseFromString(builderIDString);
                    fail("expected IllegalArgumentException");
                });
            } else {
                BuilderID trustedBuilderID = BuilderID.parseFromString(builderIDString);
                assertEquals(expectedName, trustedBuilderID.getName(), "builder name mismatch");
                assertEquals(expectedVersion, trustedBuilderID.getVersion(), "builder version mismatch");
            }
    }

    private static Stream<Arguments> parseSpecs() {
        return Stream.of(
            arguments(named("null builder", null), null, null, NullPointerException.class),
            arguments(named("valid builder with version", "some/name@v1.2.3"), "some/name", "v1.2.3", null),
            arguments(named("valid builder without version", "some/name"), "some/name", null, null),
            arguments(named("too many version tags", "some/name@vla@blo"), null, null, IllegalArgumentException.class),
            arguments(named("empty version", "some/name@"), null, null, IllegalArgumentException.class)
        );
    }
}
