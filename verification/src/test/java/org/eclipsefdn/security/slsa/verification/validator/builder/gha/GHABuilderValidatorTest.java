/*
 * Copyright (c) 2023 Eclipse Foundation. All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0, which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the
 * Eclipse Public License v. 2.0 are satisfied: GNU General Public License,
 * version 2 with the GNU Classpath Exception, which is available at
 * https://www.gnu.org/software/classpath/license.html.
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefdn.security.slsa.verification.validator.builder.gha;

import org.eclipsefdn.security.slsa.attestation.error.Errors;
import org.eclipsefdn.security.slsa.verification.ValidationCheck;
import org.eclipsefdn.security.slsa.verification.ValidationContext;
import org.eclipsefdn.security.slsa.verification.validator.builder.gha.GHABuilderValidator;
import org.eclipsefdn.security.slsa.verification.validator.builder.gha.WorkflowIdentity;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Named.named;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GHABuilderValidatorTest {

    private final GHABuilderValidator validator = new GHABuilderValidator();

    @ParameterizedTest
    @MethodSource("workflowSpecs")
    public void validateWorkflowIdentity(String sourceURI, WorkflowIdentity workflowIdentity, Collection<String> errors) {
        ValidationContext context =
            ValidationContext.builder()
                             .withSourceURI(sourceURI)
                             .build();

        validator.validateWorkflowIdentity(workflowIdentity, context);

        if (errors != null && errors.size() > 0) {
            assertEquals(errors.size(), context.getFailures().size());

            Set<String> actualErrors =
                context.getFailures()
                       .stream()
                       .map(ValidationCheck::getMessage)
                       .collect(Collectors.toSet());

            for (String error : errors) {
                assertTrue(actualErrors.contains(error), "expected error: " + error + ", got: " + actualErrors);
            }
        } else {
            assertTrue(context.getFailures().isEmpty());
        }
    }

    private static Stream<Arguments> workflowSpecs() {
        return Stream.of(
            arguments(
                "slsa/slsa-on-github-test",
                named("valid workflow identity",
                    WorkflowIdentity.of(
                    "slsa/slsa-on-github-test",
                    "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                    GHABuilderValidator.TRUSTED_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml@refs/tags/v1.2.3",
                    "workflow_dispatch",
                    GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                null
            ),

            arguments(
                "slsa/slsa-on-github-test",
                named("untrusted job workflow ref",
                    WorkflowIdentity.of(
                        "slsa/slsa-on-github-test",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        "random/workflow/ref",
                        "workflow_dispatch",
                        GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                Collections.singleton(Errors.UNTRUSTED_REUSABLE_WORKFLOW)
            ),

            arguments(
                "slsa/slsa-on-github-test",
                named("untrusted cert issuer",
                    WorkflowIdentity.of(
                        "slsa/slsa-on-github-test",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        GHABuilderValidator.TRUSTED_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml@refs/tags/v1.2.3",
                        "workflow_dispatch",
                        "https://bad.issuer.com"
                    )
                ),
                Collections.singleton(Errors.INVALID_OIDC_ISSUER)
            ),

            arguments(
                "github.com/slsa/slsa-on-github-test",
                named("source URI mismatch",
                    WorkflowIdentity.of(
                        "malicious/source",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        GHABuilderValidator.TRUSTED_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml@refs/tags/v1.2.3",
                        "workflow_dispatch",
                        GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                Collections.singleton(Errors.MISMATCH_SOURCE_URI)
            ),

            arguments(
                "slsa/slsa-on-github-test",
                named("untrusted job workflow ref",
                    WorkflowIdentity.of(
                        "slsa/slsa-on-github-test",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        "/malicious/slsa-go/.github/workflows/builder.yml@refs/tags/v1.2.3",
                        "workflow_dispatch",
                        GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                Collections.singleton(Errors.UNTRUSTED_REUSABLE_WORKFLOW)
            ),

            arguments(
                "slsa/slsa-on-github-test",
                named("untrusted job workflow ref - missing version",
                    WorkflowIdentity.of(
                        "slsa/slsa-on-github-test",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        GHABuilderValidator.TRUSTED_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml",
                        "workflow_dispatch",
                        GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                Collections.singleton(Errors.UNTRUSTED_REUSABLE_WORKFLOW_INVALID_VERSION)
            ),

            arguments(
                "slsa/slsa-on-github-test",
                named("untrusted job workflow ref - no tag / version",
                    WorkflowIdentity.of(
                        "slsa/slsa-on-github-test",
                        "0dfcd24824432c4ce587f79c918eef8fc2c44d7b",
                        GHABuilderValidator.TRUSTED_BUILDER_REPOSITORY + "/.github/workflows/builder_go_slsa3.yml@refs/head/main",
                        "workflow_dispatch",
                        GHABuilderValidator.CERTIFICATE_OIDC_ISSUER
                    )
                ),
                Collections.singleton(Errors.UNTRUSTED_REUSABLE_WORKFLOW_INVALID_VERSION)
            )
        );
    }
}
